//
//  Constants.h
//  Pandora
//
//  Created by Ankur on 17/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#ifndef Pandora_Constants_h
#define Pandora_Constants_h


// Collection Type
#define ALL             @"All"
#define BRACELETS       @"Bracelets"
#define CHARMS          @"Charms"
#define EARRINGS        @"Earrings"
#define NCKLS_PNDNTS    @"Necklaces and Pendants"
#define RINGS           @"Rings"
#define WATCHES         @"Watch"

#define CUSTOMER_SCREEN @"CustomerScreen"
#define TENDER_SCREEN @"TenderScreen"

#define APP_TYPE    @"APP_TYPE"
#define BO_APP      @"BO"
#define MPOS_APP    @"MPOS"
#define SIM_APP     @"SIM"

#define OPEN_STATUS  @"Open"
#define CLOSE_STATUS @"Close"

#define STORE_STATUS        @"StoreStatus"
#define TRANSACTION_STATUS        @"TransactionStatus"
#define SIZE                        @"Size"
#define TRANSACTION_TYPE     @"TransactionType"

#define STORE_SOURCE_WH     @"Warehouse"
#define STORE_SOURCE_SUP    @"Supplier"


#define FROM_DATE               @"FromDate"
#define TO_DATE                 @"ToDate"
#define STATUS                  @"Status"
#define ORDER_ID                @"OrderID"
#define USER                @"User"


#define SOURCE                  @"Source"

#define TRANSACTION                  @"Transaction"


#define SOURCE_TYPE             @"SourceType"
#define SOURCE_TYPE_VALUE       @"SourceTypeValue"

#define ITEM_SUB_CLASS     @"subclass"
#define ITEM_CLASS         @"class"
#define ITEM_DEPARTMENT    @"Department"
#define ITEM_SEARCH_LIMIT  @"searchlimit"


#define TASK                @"Task"
#define DATE                @"Date"
#define DESCRIPTION         @"Description"
#define TYPE                @"Type"
#define DATA                @"Data"


#define APPOINTMENTS        @"Appointment"
#define PHONE_LOGS          @"Phone Logs"

#


#endif
