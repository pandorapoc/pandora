//
//  AppDelegate.h
//  Pandora
//
//  Created by Ankur on 17/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>


#define APP_DELEGATE  [[UIApplication sharedApplication] delegate]
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic)UINavigationController *navController;
@property (strong , nonatomic)NSString *userName;
@property (strong , nonatomic)NSMutableArray *cartItemList;
@property (strong, nonatomic) NSString  *originAppType;

// Registers List

@property(strong,nonatomic)NSMutableArray *registerList;
@property(strong,nonatomic)NSMutableArray *openRegisters;

// Till info

@property(strong,nonatomic)NSString *tillId;
@property(strong,nonatomic)NSString *registerId;

@property(strong,nonatomic)NSString *reconcileTillId;

@end

