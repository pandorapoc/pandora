//
//  ViewController.m
//  Pandora
//
//  Created by Ankur on 17/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "ViewController.h"
#import "LoginViewController.h"
@interface ViewController (){

    int slide;
    NSTimer *timer;
    UIImage *image;
//    __block UIImage *imageBlured;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
  //  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    //    [ self runImageSlider];
   // });
    
    
//      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//          
//         imageBlured = [self blurWithCoreImage:[UIImage imageNamed:@"gold2.jpg"]];
//          
//     });

    //[self performSelectorInBackground:@selector(runImageSlider) withObject:nil];
    
    [self performSelector:@selector(moveToLoginViewController) withObject:nil afterDelay:2];
    // Do any additional setup after loading the view, typically from a nib.
    
    
}


- (UIImage *)blurWithCoreImage:(UIImage *)sourceImage
{
    CIImage *inputImage = [CIImage imageWithCGImage:sourceImage.CGImage];
    
    // Apply Affine-Clamp filter to stretch the image so that it does not
    // look shrunken when gaussian blur is applied
    CGAffineTransform transform = CGAffineTransformIdentity;
    CIFilter *clampFilter = [CIFilter filterWithName:@"CIAffineClamp"];
    [clampFilter setValue:inputImage forKey:@"inputImage"];
    [clampFilter setValue:[NSValue valueWithBytes:&transform objCType:@encode(CGAffineTransform)] forKey:@"inputTransform"];
    
    // Apply gaussian blur filter with radius of 30
    CIFilter *gaussianBlurFilter = [CIFilter filterWithName: @"CIGaussianBlur"];
    [gaussianBlurFilter setValue:clampFilter.outputImage forKey: @"inputImage"];
    [gaussianBlurFilter setValue:@05 forKey:@"inputRadius"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage:gaussianBlurFilter.outputImage fromRect:[inputImage extent]];
    
    // Set up output context.
    UIGraphicsBeginImageContext(self.view.frame.size);
    CGContextRef outputContext = UIGraphicsGetCurrentContext();
    
    // Invert image coordinates
    CGContextScaleCTM(outputContext, 1.0, -1.0);
    CGContextTranslateCTM(outputContext, 0, -self.view.frame.size.height);
    
    // Draw base image.
    CGContextDrawImage(outputContext, self.view.frame, cgImage);
    
    // Apply white tint
    CGContextSaveGState(outputContext);
    CGContextSetFillColorWithColor(outputContext, [UIColor colorWithWhite:1 alpha:0.2].CGColor);
    CGContextFillRect(outputContext, self.view.frame);
    CGContextRestoreGState(outputContext);
    
    // Output image is ready.
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return outputImage;
}



/*
-(void)runImageSlider{

    slide=0;
timer = [NSTimer timerWithTimeInterval:3.0
                                             target:self
                                           selector:@selector(changeSlider)
                                           userInfo:nil
                                            repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    [timer fire];
    
}
*/
-(void)viewDidDisappear:(BOOL)animated{

   // [self simepleFading];

}


/*
- (void)changeSlider
{
    slide++;
    if(slide > 6)//an array count perhaps
        slide = 1;
    //create the string as needed, example only
    NSString *topName = [NSString stringWithFormat:@"rose%d.jpg", slide];
    NSString *bottomName = [NSString stringWithFormat:@"gold%d.jpg", slide];

    UIImage * topImage = [UIImage imageNamed:topName];
    UIImage * bottomImage = [UIImage imageNamed:bottomName];

    
    [UIView transitionWithView:_imageViewTop
                      duration:2.0f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                       
                        //dispatch_async(dispatch_get_main_queue(), ^{
                            self.imageViewTop.image = topImage;
                        //    NSLog(@"Im on the main thread");
                       // });
                        

                    } completion:^(BOOL finished){NSLog(@"animation finished");}];
    
    
    [UIView transitionWithView:_imageViewBottom
                      duration:3.0f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        
                        //dispatch_async(dispatch_get_main_queue(), ^{
                        self.imageViewBottom.image = bottomImage;
                        //    NSLog(@"Im on the main thread");
                        // });
                        
                        
                    } completion:^(BOOL finished){NSLog(@"animation finished");}];
//    [UIView animateWithDuration:4.0
//                          delay:2
//                        options:UIViewAnimationOptionTransitionCrossDissolve
//                     animations:^{
//                         self.imageViewTop.image = topImage;
//                         self.imageViewBottom.image = bottomImage;                     }
//                     completion:nil];
}

*/


-(void)moveToLoginViewController{
    [self performSegueWithIdentifier:@"LoginViewSegue" sender:nil];
   // LoginViewController *myNewVC = [[LoginViewController alloc] init];
   // [self.navigationController pushViewController:myNewVC animated:YES];

}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"LoginViewSegue"]) {
        // Get destination view controller and don't forget
        // to cast it to the right class
       // LoginViewController *secondController = [segue destinationViewController];
        // Pass data
       // secondController.imageBlur = imageBlured;
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{

  //  self.navigationController.navigationBar.hidden=YES;


}
-(void)viewWillDisappear:(BOOL)animated{
  //  self.navigationController.navigationBar.hidden=NO;
    [timer invalidate];


}

- (IBAction)EnterScreen:(id)sender {
    [self moveToLoginViewController];
}

@end
