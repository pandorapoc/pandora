//
//  Helper.h
//  Pandora
//
//  Created by Ankur on 17/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//


// http://stackoverflow.com/questions/8458256/performseguewithidentifier-and-shareddata

#ifndef Pandora_Helper_h
#define Pandora_Helper_h


#endif


Ankur:

1.       Create the application structure. Please create an iPad application in Objective-C, lets call it “Pandora”. The overall architecture may be classified as follows:

Pandora

v  Analytics

v  Communication

v  Managers

v  Globals

v  Modules

o   Common

§  Controllers

§  Models

o   MPOS

§  Controllers

§  Models

o   BO

§  Controllers

§  Models

o   SIM

§  Controllers

§  Models

v  AppDelegate.h,m

v  Main.storyboard

v  Supporting Files

v  Resources

o   Images

o   Plists



2.       Create the application login, and on successful login create a container application which can switch between the different modules – MPOS, BO, SIM. The common application may also provide some functionalities and should be governed by the Single-Sign On logic.

