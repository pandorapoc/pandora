//
//  Item.h
//  Pandora
//
//  Created by Lab2 on 21/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Item : NSObject
{
    
}
@property(strong,nonatomic)NSString *itemId;
@property(strong,nonatomic)NSString *itemName;
@property(strong,nonatomic)NSString *itemType;
@property(strong,nonatomic)NSMutableDictionary *itemDesc; // contains size, metal color ,
@property(strong,nonatomic)NSString *itemPrice;



@end
