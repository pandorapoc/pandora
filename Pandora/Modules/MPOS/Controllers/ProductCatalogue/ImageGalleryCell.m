//
//  ImageGalleryCell.m
//  Pandora
//
//  Created by Ila on 14/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "ImageGalleryCell.h"



@implementation ImageGalleryCell


- (void)awakeFromNib {
    // Initialization code
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"ImageGalleryCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
    }
    
    return self;
}

-(void)updateCell {
//    NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Assets"];
//    NSString *filename = [NSString stringWithFormat:@"%@/%@", sourcePath, self.imageName];
//    
    UIImage *image = [UIImage imageNamed:self.imageName];
    
    [self.imageView1 setContentMode:UIViewContentModeScaleAspectFit];
    [self.imageView1 setImage:image];
}


@end
