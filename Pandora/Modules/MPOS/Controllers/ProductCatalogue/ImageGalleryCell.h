//
//  ImageGalleryCell.h
//  Pandora
//
//  Created by Ila on 14/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageGalleryCell : UICollectionViewCell
@property (nonatomic, strong) NSString *imageName;
@property (strong, nonatomic) IBOutlet UIImageView *imageView1;

-(void)updateCell;


@end
