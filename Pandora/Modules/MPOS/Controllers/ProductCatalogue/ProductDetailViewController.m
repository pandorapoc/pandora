//
//  ProductDetailViewController.m
//  Pandora
//
//  Created by Ila on 14/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "ImageGalleryCell.h"
#import "Constants.h"
#import "PDSIMPopOverViewController.h"

@interface ProductDetailViewController ()

@end

@implementation ProductDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self loadImages];
    [self setupCollectionView];
    sizeBtn.layer.cornerRadius=4;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark UICollectionView methods

-(void)setupCollectionView {
    [self.collectionView registerClass:[ImageGalleryCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setMinimumLineSpacing:0.0f];
    [self.collectionView setPagingEnabled:YES];
    [self.collectionView setCollectionViewLayout:flowLayout];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.dataArray count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ImageGalleryCell *cell = (ImageGalleryCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    
    NSString *imgName = [self.dataArray objectAtIndex:indexPath.row];
    
    [cell.imageView1 setImage:[UIImage imageNamed:imgName]];
    
    return cell;
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return self.collectionView.frame.size;
}


#pragma mark -
#pragma mark Data methods
-(void)loadImages {
    
    self.dataArray = [[NSMutableArray alloc]init];
    [self.dataArray addObject:@"101"];
    
    [self.dataArray addObject:@"102"];
    
    [self.dataArray addObject:@"103"];
    
    
    
//    NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Assets"];
//    self.dataArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:sourcePath error:NULL];
    
}

-(IBAction)size:(id)sender
{
    UIButton *button = (UIButton *)sender;
    PDSIMPopOverViewController *infoVC = [[PDSIMPopOverViewController alloc]initWithNibName:@"PDSIMPopOverViewController" bundle:nil];
    infoVC.type = SIZE;
    
    infoVC.simRootVC = self;
    self.popOver = [[UIPopoverController alloc]initWithContentViewController:infoVC];
    self.popOver.delegate = self;
    self.popOver.popoverContentSize = CGSizeMake(240, 150);
    [self.popOver presentPopoverFromRect:button.frame  inView:self.view permittedArrowDirections:0 animated:YES];
}

-(void)setTitleBtn:(NSString *)size
{
    
    
    [sizeBtn setTitle:size forState:UIControlStateNormal];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
