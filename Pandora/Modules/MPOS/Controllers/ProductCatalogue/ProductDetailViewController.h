//
//  ProductDetailViewController.h
//  Pandora
//
//  Created by Ila on 14/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDMPOSRootViewController.h"
#import "ProductCatalogueViewController.h"

@interface ProductDetailViewController : PDMPOSRootViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIPopoverControllerDelegate>
{
    IBOutlet UIButton *sizeBtn;
}

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) NSString *imageName;
@property(strong,nonatomic)UIPopoverController *popOver;

@property(strong, nonatomic)NSString *selectedSize;

@property(strong,nonatomic)ProductCatalogueViewController *catalogueVC;




-(IBAction)size:(id)sender;
-(void)setTitleBtn:(NSString *)size;

@end
