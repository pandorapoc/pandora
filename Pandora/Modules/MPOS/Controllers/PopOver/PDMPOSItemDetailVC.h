//
//  PDMPOSItemDetailVC.h
//  Pandora
//
//  Created by Lab2 on 21/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDMPOSCatalogueViewController.h"

@interface PDMPOSItemDetailVC : UIViewController
{
    IBOutlet UIButton *infoBtn;
    IBOutlet UIButton *detailInfoBtn;
    IBOutlet UIView *itemDesc;
    
    IBOutlet UILabel *itemName;
    IBOutlet UILabel *itemNo;
    IBOutlet UILabel *itemPrice;
    
    
    IBOutlet UILabel *descNoLbl;
    IBOutlet UILabel *metalLbl;
    IBOutlet UILabel *stoneLbl;
    IBOutlet UILabel *materialLbl;
    IBOutlet UILabel *colorLbl;
    
    IBOutlet UIImageView *detailImg;
    
    IBOutlet UIButton *cartBtn;
    
    IBOutlet UILabel *size50Lbl;
    IBOutlet UILabel *size52Lbl;
    IBOutlet UILabel *size54Lbl;
    
    
}
@property(strong, nonatomic)NSMutableDictionary *itemDetailDic;
@property(strong,nonatomic)PDMPOSCatalogueViewController *catalogueVC;
@property(strong,nonatomic)NSString *collectionType;


-(IBAction)infoBtnAction:(id)sender;
-(IBAction)detailInfoBtnAction:(id)sender;
-(IBAction)cartBtnAction:(id)sender;



@end
