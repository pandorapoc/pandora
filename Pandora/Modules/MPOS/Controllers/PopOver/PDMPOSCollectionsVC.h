//
//  PDMPOSCollectionsVC.h
//  Pandora
//
//  Created by Lab2 on 21/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDMPOSCatalogueViewController.h"

@interface PDMPOSCollectionsVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *collections;
    IBOutlet UITableView *collectionTable;
    NSUInteger checkedCell;
}

@property(strong,nonatomic)PDMPOSCatalogueViewController *catalogueVC;


@end
