//
//  PDMPOSItemDetailVC.m
//  Pandora
//
//  Created by Lab2 on 21/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDMPOSItemDetailVC.h"

@interface PDMPOSItemDetailVC ()

@end

@implementation PDMPOSItemDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [cartBtn.layer setCornerRadius:4.0];
    [self setItemDesc];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)infoBtnAction:(id)sender
{
    itemDesc.hidden = NO;
}

-(IBAction)detailInfoBtnAction:(id)sender
{
     itemDesc.hidden = YES;
}


-(void)setItemDesc
{
    if(self.itemDetailDic != nil)
    {
        
        NSString *name = [self.itemDetailDic valueForKey:@"Name"];
        NSString *number  = [self.itemDetailDic valueForKey:@"Id"];
        NSString *price  = [self.itemDetailDic valueForKey:@"Price"];
        NSString *color  = [self.itemDetailDic valueForKey:@"Color"];
        NSString *metal  = [self.itemDetailDic valueForKey:@"Metal"];
        NSString *stone  = [self.itemDetailDic valueForKey:@"Stone"];
        NSString *material  = [self.itemDetailDic valueForKey:@"Material"];
        
        itemName.text = name;
        itemNo.text = [NSString stringWithFormat:@"No.%@",number];
        descNoLbl.text = number;
        itemPrice.text = price;
        metalLbl.text = metal;
        colorLbl.text = color;
        stoneLbl.text = stone;
        materialLbl.text = material;
        
        
        size50Lbl.text = price;
        size52Lbl.text = price;
        size54Lbl.text = price;
        
        
           NSString *detailImgName = [NSString stringWithFormat:@"%@",number];
          [detailImg setImage:[UIImage imageNamed:detailImgName]];
        
        BOOL addedIncart = [[self.itemDetailDic valueForKey:@"AddedInCart"] boolValue];
        if(addedIncart)
            [cartBtn setTitle:@"Remove From Cart" forState:UIControlStateNormal];
        else
            [cartBtn setTitle:@"Add to cart" forState:UIControlStateNormal];
        
    }
}


-(IBAction)cartBtnAction:(id)sender
{
     BOOL addedIncart = [[self.itemDetailDic valueForKey:@"AddedInCart"] boolValue];
    if(addedIncart)
    {
        [self.itemDetailDic setValue:[NSNumber numberWithBool:NO] forKey:@"AddedInCart"];
        [cartBtn setTitle:@"Add to Cart" forState:UIControlStateNormal];
        [self.catalogueVC removeFromCartList:self.itemDetailDic];
    }
    else
    {
        [self.itemDetailDic setValue:[NSNumber numberWithBool:YES] forKey:@"AddedInCart"];
        [cartBtn setTitle:@"Remove from Cart" forState:UIControlStateNormal];
        [self.catalogueVC addInCartList:self.itemDetailDic];
    }
    [self.catalogueVC refreshCatalogueItems:self.collectionType];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
