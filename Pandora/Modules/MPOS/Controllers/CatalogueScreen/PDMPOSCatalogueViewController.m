//
//  PDMPOSCatalogueViewController.m
//  Pandora
//
//  Created by Lab2 on 21/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDMPOSCatalogueViewController.h"
#import "Constants.h"
#import "PDMPOSItemDetailVC.h"
#import "PDMPOSCollectionsVC.h"
#import "PDMPOSReceiptVC.h"
#import "PDMPOSCustomerBaseVC.h"
 static NSString *cellIdentifier = @"cellIndentifier";

@interface PDMPOSCatalogueViewController ()

@end

@implementation PDMPOSCatalogueViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    selectedCategory = @"";
    viewHistory.layer.cornerRadius=4;
    cancelBtn.layer.cornerRadius=4;

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [self createCustomNavigationBar];
        
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(194, 170)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 5;
   
    
    [catalogueView setCollectionViewLayout:flowLayout];
    
    [catalogueView registerNib:[UINib nibWithNibName:@"CatalogueItemCell" bundle:nil] forCellWithReuseIdentifier:cellIdentifier];
    catalogueView.pagingEnabled = YES;
    [customerBtn setTitle:appDelegate.userName forState:UIControlStateNormal];
    cartItemList = [[NSMutableArray alloc]init];
    
    customerLoyalityPopup.layer.cornerRadius=4;
    customerLoyalityPopup.layer.masksToBounds = YES;
    
    
}



-(void)viewWillAppear:(BOOL)animated
{
    isSearching = FALSE;
     [self getItemList:ALL];
    double pages = [itemList count]/15.0 ;
    int noOfPages = ceil(pages);
    pageControl.numberOfPages = noOfPages;
    [catalogueView reloadData];
    [self setNavigationBar];

}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setNavigationBar
{
    //NSArray *subViews = searchBar.s;
}
-(void)rightBarButtonAction:(id)sender
{
    PDMPOSCustomerBaseVC *tenderVC = [[PDMPOSCustomerBaseVC alloc]initWithNibName:@"PDMPOSCustomerBaseVC" bundle:nil];
    tenderVC.screenType = TENDER_SCREEN;
    tenderVC.itemsInCart = cartItemList;
    [self.navigationController pushViewController:tenderVC animated:NO];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/





-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(isSearching)
        return [searchList count];
    return [itemList count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
   
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];

    
    NSArray *uiViews = [cell.contentView subviews];
    NSMutableDictionary *itemDic = nil;
    if(isSearching)
           itemDic = [searchList objectAtIndex:indexPath.row];
   else
       itemDic = [itemList objectAtIndex:indexPath.row];
    
    NSString *imgId = [itemDic valueForKey:@"Id" ];
    NSString *price = [itemDic valueForKey:@"Price"];
    NSNumber *addedIncart = [itemDic valueForKey:@"AddedInCart"];
    for (UIView *view in uiViews)
    {
        if([view isKindOfClass:[UIImageView class]] && view.tag == 1)
        {
            UIImageView *cellImg = (UIImageView *)view;
            [cellImg setImage:[UIImage imageNamed:imgId]];
        }
        if([view isKindOfClass:[UILabel class]] && view.tag == 2)
        {
            UILabel *lbl = (UILabel *)view;
            lbl.text = price;
        }
        if([view isKindOfClass:[UIButton class]] )
        {
            UIButton *btn = (UIButton *)view;
            btn.tag = indexPath.row;
            [btn addTarget:self action:@selector(cartBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            if([addedIncart boolValue])
            {
                [btn setImage:[UIImage imageNamed:@"cart_press_icon"] forState:UIControlStateNormal];
            }
            else
            {
                 [btn setImage:[UIImage imageNamed:@"cart_icon"] forState:UIControlStateNormal];
            }
        }
    }
    
    
    cell.backgroundColor = [UIColor whiteColor];
    cell.layer.cornerRadius = 4.0;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableDictionary *dic = [itemList objectAtIndex:indexPath.row];
    
    PDMPOSItemDetailVC *itemDetailVC = [[PDMPOSItemDetailVC alloc]initWithNibName:@"PDMPOSItemDetailVC" bundle:nil];
    itemDetailVC.itemDetailDic = dic;
    itemDetailVC.collectionType = selectedCategory;
    itemDetailVC.catalogueVC = self;
    self.itemDetailPopover = [[UIPopoverController alloc]initWithContentViewController:itemDetailVC];
     self.itemDetailPopover.popoverContentSize = CGSizeMake(920.0, 550);
    

    CGRect rect = CGRectMake(50,170,920.0, 550.0);
    
    [ self.itemDetailPopover presentPopoverFromRect:rect  inView:self.view permittedArrowDirections:0 animated:YES];
    
    
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
//    //find the page number you are on
    CGFloat pageWidth = catalogueView.frame.size.width;
    int page = ceil((catalogueView.contentOffset.x - pageWidth / 2) / pageWidth) ;
    NSLog(@"Scrolling - You are now on page %i",page);
    pageControl.currentPage = page;
}


-(void)cartBtnAction:(id)sender
{
    UIButton *cartBtn = (UIButton *)sender;
    int index = cartBtn.tag;
    NSMutableDictionary *dic = [itemList objectAtIndex:index];
     NSNumber *addedIncart = [dic valueForKey:@"AddedInCart"];
    if([addedIncart boolValue])
    {
        [dic setValue:[NSNumber numberWithBool:NO] forKey:@"AddedInCart"];
        [self removeFromCartList:dic];
    }
    else
    {
        [self addInCartList:dic];
         [dic setValue:[NSNumber numberWithBool:YES] forKey:@"AddedInCart"];
    }
    [catalogueView reloadData];
}

-(NSArray *)parseFromFile
{
    NSString *file = [[NSBundle mainBundle] pathForResource:@"ItemList" ofType:@"plist"];
    NSMutableArray *data = [NSMutableArray arrayWithContentsOfFile:file];
    NSLog(@"%i", data.count);
    return data;
}


-(void )getItemList:(NSString *)collectionType
{
   
    if(dataArray == nil || [dataArray count] == 0)
    {
        dataArray = [self parseFromFile];
    }
    if(![selectedCategory isEqualToString:collectionType])
    {
        if(itemList == nil)
            itemList = [[NSMutableArray alloc]init];
        [itemList removeAllObjects];
        for (NSMutableDictionary *dic in dataArray)
        {
            if([collectionType isEqualToString:ALL] || [collectionType isEqualToString:@""])
            {
                [itemList addObjectsFromArray:[dic valueForKey:@"Data"]];
            }
            else if([collectionType isEqualToString:[dic valueForKey:@"Type"]])
            {
                [itemList addObjectsFromArray:[dic valueForKey:@"Data"]];
            }
        }
    }
    selectedCategory = collectionType;
   
    
}


-(NSMutableArray *)getCatagoryData:(NSString *)collectionType
{
    NSMutableArray *dataList = [[NSMutableArray alloc]init];
    
    return dataList;
}

-(void)collectionBtnAction:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    PDMPOSCollectionsVC *collectionVC = [[PDMPOSCollectionsVC alloc]initWithNibName:@"PDMPOSCollectionsVC" bundle:nil];
    self.collectionPopover = [[UIPopoverController alloc]initWithContentViewController:collectionVC];
    self.collectionPopover.popoverContentSize = CGSizeMake(370.0, 380.0);
    collectionVC.catalogueVC = self;
    [self.collectionPopover presentPopoverFromRect:btn.frame  inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
    
}


-(void)refreshCatalogueItems:(NSString *)collectionType
{
    [self.collectionPopover dismissPopoverAnimated:YES];
     itemLbl.hidden = YES;
     isSearching = FALSE;
     catalogueView.hidden = NO;
   [self getItemList:collectionType];
    titleLbl.text = collectionType;
    double pages = [itemList count]/15.0 ;
    int noOfPages = ceil(pages);
    pageControl.numberOfPages = noOfPages;
     [catalogueView reloadData];
    
}


-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchbar
{
    //self.addButton.enabled = NO;
    isSearching = TRUE;
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchbar
{
    if(searchbar.text.length == 0)
    {
        isSearching = FALSE;
         itemLbl.hidden = YES;
        [self refreshCatalogueItems:selectedCategory];
        [searchbar resignFirstResponder];
    }
}

-(void)searchBar:(UISearchBar *)searchbar textDidChange:(NSString *)searchText
{
    if ([searchText length] == 0)
    {
        [searchbar performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0];
    }
    else
    {
        searchStr = searchText;
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchbar
{
    if(searchList == nil)
    {
        searchList = [[NSMutableArray alloc]init];
    }
    [searchList removeAllObjects];
    
    if(itemList != nil && [itemList count] > 0)
    {
        for (NSMutableDictionary *dic in itemList)
        {
            NSString *itemName = [dic valueForKey:@"Name"];
           if (!([itemName rangeOfString:searchStr options:NSCaseInsensitiveSearch].location == NSNotFound))
            {
                [searchList addObject:dic];
            }
        }
    }
    [searchbar resignFirstResponder];
    if([searchList count] > 0)
    {
         itemLbl.hidden = YES;
          catalogueView.hidden = NO;
        double pages = [searchList count]/15.0 ;
        int noOfPages = ceil(pages);
        pageControl.numberOfPages = noOfPages;
        [catalogueView reloadData];
        
    }
    else
    {
        catalogueView.hidden = YES;
        itemLbl.hidden = NO;
        pageControl.numberOfPages = 0;
    }
}

-(IBAction)customerBtnAction:(id)sender
{
    
    [self addPopOverViewToTopLayer];
}

-(void)addPopOverViewToTopLayer
{
   
    [self.view addSubview:customerLoyalityPopup];
    customerLoyalityPopup.hidden=NO;
}

- (IBAction)viewHistoryBtnClicked:(id)sender
{
   
    
    NSString *loyalityStr = loyalityTF.text;
    if([loyalityStr isEqualToString:@""])
    {
        UIAlertView *blankAlert = [[UIAlertView alloc]initWithTitle:nil message:@"Please Enter Customer Loyality Number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [blankAlert show];
    }
    else
    {
         customerLoyalityPopup.hidden=YES;
        PDMPOSCustomerBaseVC *customerVC = [[PDMPOSCustomerBaseVC alloc]initWithNibName:@"PDMPOSCustomerBaseVC" bundle:nil];
        customerVC.screenType = CUSTOMER_SCREEN;
        customerVC.customerLoyalityNum = loyalityStr;
    
        [self.navigationController pushViewController:customerVC animated:YES];
    }
    
}

- (IBAction)CancelBtnClicked:(UIButton *)sender {
    
    customerLoyalityPopup.hidden=YES;
}

-(void)addInCartList:(NSMutableDictionary *)itemDic
{
    [cartItemList addObject:itemDic];
    appDelegate.cartItemList = cartItemList;
}

-(void)removeFromCartList:(NSMutableDictionary *)itemDic
{
    [cartItemList removeObject:itemDic];
    appDelegate.cartItemList = cartItemList;
}
@end
