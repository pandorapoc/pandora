//
//  PDMPOSCatalogueViewController.h
//  Pandora
//
//  Created by Lab2 on 21/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDMPOSRootViewController.h"
#import "Item.h"




@interface PDMPOSCatalogueViewController :PDMPOSRootViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    IBOutlet UICollectionView *catalogueView;
    IBOutlet UIPageControl *pageControl;
    IBOutlet UIButton *customerBtn;
    IBOutlet UILabel *itemLbl;
    
    NSString *selectedCategory;
    
    NSMutableArray *itemList;
    NSArray *dataArray;
    NSMutableArray *cartItemList;
    
    NSMutableArray *searchList;
    BOOL isSearching;
    NSString *searchStr;
    
    IBOutlet UIView *customerLoyalityPopup;
    IBOutlet UITextField *loyalityTF;
    
    IBOutlet UIButton *viewHistory;
    IBOutlet UIButton *cancelBtn;
}

@property(strong,nonatomic)UIPopoverController *collectionPopover;
@property(strong,nonatomic)UIPopoverController *itemDetailPopover;


-(IBAction)customerBtnAction:(id)sender;
- (IBAction)viewHistoryBtnClicked:(id)sender;
- (IBAction)CancelBtnClicked:(UIButton *)sender;


-(void)refreshCatalogueItems:(NSString *)collectionType;
-(void)addInCartList:(NSMutableDictionary *)itemDic;

-(void)removeFromCartList:(NSMutableDictionary *)itemDic;
@end
