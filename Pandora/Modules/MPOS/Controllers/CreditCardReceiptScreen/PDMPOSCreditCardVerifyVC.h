//
//  PDMPOSCreditCardVerifyVC.h
//  Pandora
//
//  Created by Lab2 on 29/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDMPOSRootViewController.h"

@interface PDMPOSCreditCardVerifyVC : PDMPOSRootViewController
{
    IBOutlet UIButton *cancelBtn;
    IBOutlet UIButton *verifyBtn;
    
    
}


-(IBAction)cancelBtnAction:(id)sender;
-(IBAction)verifyBtnAction:(id)sender;
@end
