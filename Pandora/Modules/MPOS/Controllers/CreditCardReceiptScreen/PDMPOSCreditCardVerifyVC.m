//
//  PDMPOSCreditCardVerifyVC.m
//  Pandora
//
//  Created by Lab2 on 29/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDMPOSCreditCardVerifyVC.h"
#import "PDMPOSReceiptVC.h"
#import "BezierPathView.h"
@interface PDMPOSCreditCardVerifyVC (){
    
    IBOutlet BezierPathView *signatureView;
}

@end

@implementation PDMPOSCreditCardVerifyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [self createCustomNavigationBar];
    signatureView.layer.cornerRadius = 5;
    cancelBtn.layer.cornerRadius=4;
    verifyBtn.layer.cornerRadius=4;
    signatureView.layer.masksToBounds = YES;
    [self setNavigationBar];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)cancelBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}
-(IBAction)verifyBtnAction:(id)sender
{
    PDMPOSReceiptVC *receiptVC = [[PDMPOSReceiptVC alloc] initWithNibName:@"PDMPOSReceiptVC" bundle:nil];
    
    UIGraphicsBeginImageContextWithOptions(signatureView.bounds.size, NO, [UIScreen mainScreen].scale);
    [signatureView drawViewHierarchyInRect:signatureView.bounds afterScreenUpdates:YES];
    UIImage *signatureImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    receiptVC.signImg = signatureImage ;
    [self.navigationController pushViewController:receiptVC animated:NO];
}

-(void)setNavigationBar
{
    UIButton *leftBtn = [[UIButton alloc ]initWithFrame:CGRectMake(5, 20, 200, 40)];
    
    
    [leftBtn setImage:[UIImage imageNamed: @"back_arrow"] forState:UIControlStateNormal];
    [leftBtn setTitle:@"Tender" forState:UIControlStateNormal];
    [leftBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [leftBtn addTarget:self action:@selector(leftBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:leftBtn];
    
    
    
    leftSlideBarBtn.hidden = YES;
    leftBarTitleBtn.hidden = YES;
    
    titleLbl.text = @"Credit Card";
    searchBar.hidden = YES;
    rightBarBtn.hidden = YES;
   
}

-(void)leftBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
