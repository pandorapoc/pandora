//
//  PDMPOSCreditCardSwipeVC.m
//  Pandora
//
//  Created by Lab2 on 21/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDMPOSCreditCardSwipeVC.h"
#import "PDMPOSCreditCardVerifyVC.h"

@interface PDMPOSCreditCardSwipeVC ()

@end

@implementation PDMPOSCreditCardSwipeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    cardNumberView.layer.cornerRadius = 5.0f;
    cardNumberView.layer.masksToBounds = YES;
    
    pinView.layer.cornerRadius = 5.0f;
    pinView.layer.masksToBounds = YES;

    nextBtn.layer.cornerRadius = 5.0f;
    nextBtn.layer.masksToBounds = YES;

    [self createCustomNavigationBar];
    [self setNavigationBar];
    datePicker.backgroundColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)setNavigationBar
{
    UIButton *leftBtn = [[UIButton alloc ]initWithFrame:CGRectMake(5, 20, 200, 40)];
    
   
    [leftBtn setImage:[UIImage imageNamed: @"back_arrow"] forState:UIControlStateNormal];
    [leftBtn setTitle:@"Tender" forState:UIControlStateNormal];
    [leftBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [leftBtn addTarget:self action:@selector(leftBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:leftBtn];
    
   
    leftSlideBarBtn.hidden = YES;
    leftBarTitleBtn.hidden = YES;
    
    titleLbl.text = @"Credit Card";
    searchBar.hidden = YES;
    rightBarBtn.hidden = YES;
}

-(void)leftBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)nextBtnAction:(id)sender
{
    PDMPOSCreditCardVerifyVC *swipeVC = [[PDMPOSCreditCardVerifyVC alloc] initWithNibName:@"PDMPOSCreditCardVerifyVC" bundle:nil];
    
    [self.navigationController pushViewController:swipeVC animated:NO];
}

-(IBAction)dateBtnAction:(id)sender
{
    datePicker.hidden = NO;
    [self.view endEditing:YES];
}

- (IBAction)datePickerAction:(UIDatePicker *)datepicker
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/YYYY"];
    NSString *strDate = [dateFormatter stringFromDate:datepicker.date];
    [dateBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [dateBtn setTitle:strDate forState:UIControlStateNormal];
    datePicker.hidden = YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldBeginEditing");
    if(textField == pinFeild)
        containerView.frame = CGRectMake(211, 100, 595, 330);
    datePicker.hidden = YES;
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
     containerView.frame = CGRectMake(211, 130, 595, 330);
    NSLog(@"textFieldDidBeginEditing");
}

-(void)resizeContainerView
{
    
}
@end
