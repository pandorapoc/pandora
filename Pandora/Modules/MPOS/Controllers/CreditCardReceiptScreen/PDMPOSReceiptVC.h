//
//  PDMPOSReceiptVC.h
//  Pandora
//
//  Created by Lab2 on 21/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDMPOSRootViewController.h"

@interface PDMPOSReceiptVC : PDMPOSRootViewController<UITableViewDataSource,UITableViewDelegate>
{
   
    IBOutlet UILabel *name;
    IBOutlet UITableView *itemTable;
    IBOutlet UIImageView *signImgView;
    
    IBOutlet UILabel *subTotal;
    IBOutlet UILabel *total;
    IBOutlet UILabel *creditCard;
    IBOutlet UILabel *tax;
    
    
    IBOutlet UIView *baseView;
    IBOutlet UIButton *printBtn;
    IBOutlet UIButton *emailBtn;
    
}

@property(strong,nonatomic) UIImage *signImg ;
@property(strong,nonatomic)NSMutableArray *itemList;

@end
