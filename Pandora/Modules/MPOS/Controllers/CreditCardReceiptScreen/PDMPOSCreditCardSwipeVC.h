//
//  PDMPOSCreditCardSwipeVC.h
//  Pandora
//
//  Created by Lab2 on 21/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDMPOSRootViewController.h"

@interface PDMPOSCreditCardSwipeVC : PDMPOSRootViewController<UITextFieldDelegate>
{
    IBOutlet UIButton *nextBtn;
    IBOutlet UIView *cardNumberView;
    IBOutlet UIView *dateView;
    IBOutlet UIView *pinView;
    IBOutlet UITextField *pinFeild;
    IBOutlet UIDatePicker *datePicker;
    IBOutlet UIView *containerView;
    
    IBOutlet UIButton *dateBtn;
}



-(IBAction)nextBtnAction:(id)sender;
-(IBAction)dateBtnAction:(id)sender;

-(IBAction)datePickerAction:(id)sender;
@end
