//
//  PDMPOSReceiptVC.m
//  Pandora
//
//  Created by Lab2 on 21/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDMPOSReceiptVC.h"

@interface PDMPOSReceiptVC ()

@end

@implementation PDMPOSReceiptVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
     [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [self createCustomNavigationBar];
     [self setNavigationBar];
    self.itemList = appDelegate.cartItemList;
    [self setItemDetailPrice];
    
    baseView.layer.cornerRadius = 4;
    baseView.layer.masksToBounds = YES;
    
    signImgView.layer.cornerRadius = 4;
    signImgView.layer.masksToBounds = YES;
    
    printBtn.layer.cornerRadius = 4;
    printBtn.layer.masksToBounds = YES;
    
    emailBtn.layer.cornerRadius = 4;
    emailBtn.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setNavigationBar
{
    UIButton *leftBtn = [[UIButton alloc ]initWithFrame:CGRectMake(5, 20, 200, 40)];
    
    
    [leftBtn setImage:[UIImage imageNamed: @"back_arrow"] forState:UIControlStateNormal];
    [leftBtn setTitle:@"Tender" forState:UIControlStateNormal];
    [leftBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [leftBtn addTarget:self action:@selector(leftBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:leftBtn];
    
    
    
    leftSlideBarBtn.hidden = YES;
    leftBarTitleBtn.hidden = YES;
    
    titleLbl.text = @"Credit Card";
    searchBar.hidden = YES;
    rightBarBtn.hidden = YES;
    signImgView.image = self.signImg;
}

-(void)leftBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60.0;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[[NSBundle mainBundle]loadNibNamed:@"ReceiptTableHeaderView" owner:self options:nil]objectAtIndex:0];;
}

#pragma mark UITableview datasource and delegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 90;
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.itemList count];    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"ReceiptItemcell" owner:self options:nil]objectAtIndex:0];
    }
    NSMutableDictionary *dic = [self.itemList objectAtIndex:indexPath.row];
    NSString *itemName = [dic valueForKey:@"Name"];
    NSString *itemId = [dic valueForKey:@"Id"];
    NSString *price = [dic valueForKey:@"Price"];
    
    NSArray *subViews = [cell.contentView subviews];
    
    for (UIView *view in subViews)
    {
        if([view isKindOfClass:[UILabel class]] && view.tag == 1)
        {
            UILabel *lbl = (UILabel *)view;
            lbl.text = [NSString stringWithFormat:@"%d",(indexPath.row+1)];
        }
        else if([view isKindOfClass:[UILabel class]] && view.tag == 2)
        {
            UILabel *lbl = (UILabel *)view;
            lbl.text = itemName;
        }
        else if([view isKindOfClass:[UILabel class]] && view.tag == 3)
        {
            UILabel *lbl = (UILabel *)view;
            lbl.text = [NSString stringWithFormat:@"No.%@",itemId];
        }
        else if([view isKindOfClass:[UILabel class]] && view.tag == 4)
        {
            UILabel *lbl = (UILabel *)view;
            lbl.text = price;
        }
        else if([view isKindOfClass:[UILabel class]] && view.tag == 5)
        {
            UILabel *lbl = (UILabel *)view;
             lbl.text = price;
        }
        
    }
 
    return cell;
}


-(void)setItemDetailPrice
{
    if(self.itemList != nil && [self.itemList count] > 0)
    {
        NSInteger price = 0;
        for (NSMutableDictionary *dic in self.itemList)
        {
            NSString *itemPrice = [dic valueForKey:@"Price"];
            price += [itemPrice integerValue];
        }
        subTotal.text = [NSString stringWithFormat:@"$%d",price];
        NSString *taxvalue = tax.text;
        tax.text = [NSString stringWithFormat:@"$%@",taxvalue];
        total.text =[NSString stringWithFormat:@"$%d",(price + [taxvalue integerValue])] ;
        creditCard.text = total.text;
        
    }
}
@end
