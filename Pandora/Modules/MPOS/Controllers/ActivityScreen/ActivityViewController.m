//
//  ActivityViewController.m
//  Pandora
//
//  Created by Lab2 on 14/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "ActivityViewController.h"
#import "ActivityTableViewCell.h"
#import "Constants.h"



@interface ActivityViewController ()

@end

@implementation ActivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];

    // Do any additional setup after loading the view from its nib.
    dataList = [self parseFromFile];
    [activityTV reloadData];
    [self createCustomNavigationBar];
    [self setNavigationBar];
    
    appsBtn.layer.cornerRadius = 4;
    appsBtn.layer.masksToBounds = YES;
    plusBtn.layer.cornerRadius = 4;
    plusBtn.layer.masksToBounds = YES;
    deleteBtn.layer.cornerRadius = 4;
    deleteBtn.layer.masksToBounds = 4;
    msgBtn.layer.cornerRadius = 4;
    msgBtn.layer.masksToBounds = 4;
    
}


-(void)setNavigationBar
{
    
    
    
    
    //leftSlideBarBtn.hidden = YES;
    leftBarTitleBtn.hidden = YES;
    
    titleLbl.text = @"Activity";
    searchBar.hidden = YES;
    rightBarBtn.hidden = YES;
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSMutableArray *)parseFromFile
{
    NSString *file = [[NSBundle mainBundle] pathForResource:@"ActivityList" ofType:@"plist"];
    NSMutableArray *data = [NSMutableArray arrayWithContentsOfFile:file];
    NSLog(@"%lu", data.count);
    return data;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     Get the new view controller using [segue destinationViewController].
     Pass the selected object to the new view controller.
}
*/

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45.0;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionView = [[UIView alloc]initWithFrame:CGRectMake(0, -20, 989, 45)];
    
    //sectionView.backgroundColor = [UIColor colorWithRed:173.0/255 green:141.0/255 blue:192.0/255 alpha:1];
    sectionView.backgroundColor = [UIColor colorWithRed:60.0/255 green:49.0/255 blue:66.0/255 alpha:1];
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 5, 200, 40)];
    lbl.textColor = [UIColor whiteColor];
     NSMutableArray *dic = [dataList objectAtIndex:section];
    lbl.text = [dic valueForKey:TYPE];
    
    
    [sectionView addSubview:lbl];
    return sectionView;

}
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, 200, 40)];
//    NSMutableArray *dic = [dataList objectAtIndex:section];
//    return  [dic valueForKey:TYPE];
//}

#pragma mark UITableview datasource and delegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 130;
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [dataList count];    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    NSMutableArray *dic = [dataList objectAtIndex:section];
    NSMutableArray *dataArray = [dic valueForKey:DATA];

    return [dataArray count];    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ActivityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"ActivityTableViewCell" owner:self options:nil]objectAtIndex:0];
        cell.backgroundColor = [UIColor clearColor];
        cell.cellBg.layer.cornerRadius = 4;
        cell.status.layer.cornerRadius = 8;
        cell.status.layer.masksToBounds = YES;
      
    }
    NSMutableDictionary *dic = [dataList objectAtIndex:indexPath.section];
    
    NSMutableArray *dataArray = [dic valueForKey:DATA];
    
    NSMutableDictionary *dataDic = [dataArray objectAtIndex:indexPath.row];
    
    cell.taskLbl.text = [dataDic valueForKey:TASK];
    cell.date.text = [dataDic valueForKey:DATE];
    cell.taskDesc.text = [dataDic valueForKey:DESCRIPTION];
    cell.status.text = [dataDic valueForKey:STATUS];

    return cell;
}


@end
