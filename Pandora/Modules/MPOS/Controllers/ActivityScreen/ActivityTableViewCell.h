//
//  ActivityTableViewCell.h
//  Pandora
//
//  Created by Lab2 on 14/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityTableViewCell : UITableViewCell
{
    
}
@property(strong,nonatomic)IBOutlet UILabel *taskLbl;
@property(strong,nonatomic)IBOutlet UILabel *date;
@property(strong,nonatomic)IBOutlet UILabel *taskDesc;
@property(strong,nonatomic)IBOutlet UILabel *status;
@property(strong,nonatomic)IBOutlet UIView *cellBg;
@end
