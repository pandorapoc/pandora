//
//  ActivityViewController.h
//  Pandora
//
//  Created by Lab2 on 14/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDMPOSRootViewController.h"

@interface ActivityViewController :PDMPOSRootViewController <UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView *activityTV;
    NSMutableArray *dataList;
    
    IBOutlet UIButton *appsBtn;
    IBOutlet UIButton *plusBtn;
    IBOutlet UIButton *deleteBtn;
    IBOutlet UIButton *msgBtn;
}
@end
