//
//  BaseTableViewCell.m
//  Pandora
//
//  Created by Ankur on 29/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "BaseTableViewCell.h"

@implementation BaseTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
