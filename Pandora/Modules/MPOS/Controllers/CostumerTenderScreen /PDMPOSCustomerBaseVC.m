//
//  PDMPOSCustomerBaseVC.m
//  Pandora
//
//  Created by Lab2 on 21/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDMPOSCustomerBaseVC.h"
#import <QuartzCore/QuartzCore.h>
#import "BaseTableViewCell.h"
#import "PDMPOSCreditCardSwipeVC.h"




@interface PDMPOSCustomerBaseVC ()<UIAlertViewDelegate>

@end

@implementation PDMPOSCustomerBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
     [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    // Do any additional setup after loading the view from its nib.
//    UIImage *slideImage = [UIImage imageNamed:@"slide_tab"];
//    UIButton *faceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [faceBtn addTarget:self action:@selector(presentLeftMenuViewController:) forControlEvents:UIControlEventTouchUpInside];
//    faceBtn.bounds = CGRectMake( 0, 0, slideImage.size.width, slideImage.size.height );
//    [faceBtn setImage:slideImage forState:UIControlStateNormal];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:faceBtn];;
//    [self.navigationController.navigationBar setTintColor:[UIColor blackColor]];

    makeTransBtn.layer.cornerRadius = 4.0f; // this value vary as per your desire
    makeTransBtn.clipsToBounds = YES;
    modifyBtn.layer.cornerRadius = 4.0f; // this value vary as per your desire
    modifyBtn.clipsToBounds = YES;

    
    [self.view addSubview:_actionView];
    _actionView.frame=CGRectMake(_actionView.frame.origin.x,self.view.frame.size.height, _actionView.frame.size.width, _actionView.frame.size.height);

    [self.view addSubview:_actionViewPayment];
    _actionViewPayment.frame=CGRectMake(_actionViewPayment.frame.origin.x,self.view.frame.size.height, _actionViewPayment.frame.size.width, _actionViewPayment.frame.size.height);

    
   
    _discountPercentageView.layer.cornerRadius=4;
    _discountDollarView.layer.cornerRadius=4;
    _applyGiftView.layer.cornerRadius=4;
    _tableheaderView.layer.cornerRadius=4;
    invoiceDetailView.layer.cornerRadius = 8;
    invoiceDetailView.layer.masksToBounds = YES;
    
    [self createCustomNavigationBar];
    [self setNavigationBarItem];
    customerName.text = appDelegate.userName;
    if(self.customerLoyalityNum == nil || [self.customerLoyalityNum isEqualToString:@""])
        self.customerLoyalityNum = @"7676756674";
    loyalityLbl.text = [NSString stringWithFormat:@"#%@",self.customerLoyalityNum];
    
    [self setInvoiceDetails];
   // _commonTableView.tableHeaderView=_tableheaderView;
    for (UIButton *btn in roundedBtnCollections) {
        btn.layer.cornerRadius=4;
    }
    
}


-(void)setInvoiceDetails
{
    if(self.itemsInCart != nil && [self.itemsInCart count] > 0)
    {
        numOfItems.text = [NSString stringWithFormat:@"%d",[self.itemsInCart count]];
        
        NSInteger itemPice = 0;
        for (NSMutableDictionary *dic in self.itemsInCart)
        {
            NSString *price = [dic valueForKey:@"Price"];
            itemPice += [price integerValue];
        }
        subTotal.text = [NSString stringWithFormat:@"%d",itemPice];
        NSUInteger subTotalValue = [subTotal.text integerValue];
        NSUInteger shippingValue = [shipping.text integerValue];
        NSUInteger taxValue = [tax.text integerValue];
        total.text = [NSString stringWithFormat:@"%d",(subTotalValue+shippingValue+taxValue)];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)leftBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setNavigationBarItem
{
    
    UIButton *leftBtn = [[UIButton alloc ]initWithFrame:CGRectMake(5, 20, 200, 40)];
    
    
    [leftBtn setImage:[UIImage imageNamed: @"back_arrow"] forState:UIControlStateNormal];
    [leftBtn setTitle:@"Collections" forState:UIControlStateNormal];
    [leftBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [leftBtn addTarget:self action:@selector(leftBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:leftBtn];
    
    
    leftSlideBarBtn.hidden = YES;
    leftBarTitleBtn.hidden = YES;
    if([self.screenType isEqualToString: TENDER_SCREEN])
    {
        titleLbl.text = @"Tender";
        modifyBtn.hidden = NO;
        makeTransBtn.hidden = NO;
    }
    else
    {
         titleLbl.text = @"Customer History";
        modifyBtn.hidden = YES;
        makeTransBtn.hidden = YES;
    }
    searchBar.hidden = YES;
    [rightBarBtn setTitle:@"Logout" forState:UIControlStateNormal];
}

-(void)rightBarButtonAction:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)modifyBtnClicked:(UIButton *)sender {
    
    [self resetAnimatedActionViewPayment];

    _actionView.hidden=NO;

            [UIView animateWithDuration:0.5
                                 delay:0
                                options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                
                                _actionView.frame=CGRectMake(_actionView.frame.origin.x,self.view.frame.size.height-_actionView.frame.size.height, _actionView.frame.size.width, _actionView.frame.size.height);
                                                      }
                             completion:^(BOOL finished){
                                
                                 NSLog(@"action View animation completed");
                                // [self resetAnimatedActionView];
                                 
                             }];
    
}

- (IBAction)makePaymentBtnClicked:(UIButton *)sender {
    
     [self resetAnimatedActionView];

    _actionViewPayment.hidden=NO;
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionTransitionCrossDissolve
                     animations:^{
                         
                         _actionViewPayment.frame=CGRectMake(_actionViewPayment.frame.origin.x,self.view.frame.size.height-_actionViewPayment.frame.size.height, _actionViewPayment.frame.size.width, _actionViewPayment.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         
                         NSLog(@"makePayment animation completed");
                         // [self resetAnimatedActionView];
                         
                     }];
    

}



-(void)resetAnimatedActionView{

    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionTransitionCrossDissolve
                     animations:^{
                         
                         _actionView.frame=CGRectMake(_actionView.frame.origin.x,self.view.frame.size.height, _actionView.frame.size.width, _actionView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         
                         NSLog(@"resetAnimatedActionView completed");
                         _actionView.hidden=YES;
                     }];
}


-(void)resetAnimatedActionViewPayment{
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionTransitionCrossDissolve
                     animations:^{
                         
                         _actionViewPayment.frame=CGRectMake(_actionViewPayment.frame.origin.x,self.view.frame.size.height, _actionViewPayment.frame.size.width, _actionViewPayment.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         
                         NSLog(@"resetAnimatedActionViewPayment completed");
                         _actionViewPayment.hidden=YES;
                     }];
}



- (IBAction)applyGiftCardBtnClicked:(UIButton *)sender{
    [self resetAnimatedActionView];
     [self addPopOverViewToTopLayer];
    _popOverView.hidden=NO;
    _discountDollarView.hidden=YES;
    _discountPercentageView.hidden=YES;
    _applyGiftView.hidden=NO;

}
- (IBAction)applyEmployeeDiscountBtnClicked:(UIButton *)sender{
//    [self resetAnimatedActionView];
//    _popOverView.hidden=NO;
//    _discountDollarView.hidden=YES;
//    _discountPercentageView.hidden=YES;
//    _applyGiftView.hidden=YES;


}
- (IBAction)postVoidBtnClicked:(UIButton *)sender{
//    [self resetAnimatedActionView];
//    _popOverView.hidden=NO;
//    _discountDollarView.hidden=YES;
//    _discountPercentageView.hidden=YES;
//    _applyGiftView.hidden=YES;


}
- (IBAction)retrieveBtnClicked:(UIButton *)sender{
//    [self resetAnimatedActionView];
//    _popOverView.hidden=NO;
//    _discountDollarView.hidden=YES;
//    _discountPercentageView.hidden=YES;
//    _applyGiftView.hidden=YES;


}
- (IBAction)transactionDollarDiscountBtnClicked:(UIButton *)sender{
    [self resetAnimatedActionView];
     [self addPopOverViewToTopLayer];
    _popOverView.hidden=NO;
    _discountDollarView.hidden=NO;
    _discountPercentageView.hidden=YES;
    _applyGiftView.hidden=YES;

}
- (IBAction)transactionPercentageDiscountBtnClicked:(UIButton *)sender{
    [self resetAnimatedActionView];
     [self addPopOverViewToTopLayer];
    _popOverView.hidden=NO;
    _discountDollarView.hidden=YES;
    _discountPercentageView.hidden=NO;
    _applyGiftView.hidden=YES;

}
- (IBAction)suspendBtnClicked:(UIButton *)sender
{
      [self addPopOverViewToTopLayer];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Do you want to suspend transaction?"
                                                    message:@"The information will be saved and you can continue the transaction at a later time."
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes",nil];
    [alert show];
    
    // If you're not using ARC, you will need to release the alert view.
    // [alert release];
    _popOverView.hidden=NO;
    [self resetAnimatedActionView];

}



- (IBAction)cancelBtnClicked:(UIButton *)sender{
    [self resetAnimatedActionView];

}

- (IBAction)creditCardBtnClicked:(id)sender {
    PDMPOSCreditCardSwipeVC *customerBaseVC = [[PDMPOSCreditCardSwipeVC alloc]initWithNibName:@"PDMPOSCreditCardSwipeVC" bundle:nil];
    [self.navigationController pushViewController:customerBaseVC animated:YES];

}

- (IBAction)debitCardBtnClicked:(id)sender {
    [self resetAnimatedActionViewPayment];

}

- (IBAction)changeTerminalBtnClicked:(id)sender
{
    chooseTerminalPaymentLabel.text=@"Select Payment Terminals";
    for (UIView *obj in choosePaymentBtnsCollections) {
        obj.hidden=YES;
    }
    changeTerminalView.hidden=NO;

}

- (IBAction)processBtnClicked:(id)sender {
    [self resetAnimatedActionViewPayment];
    changeTerminalView.hidden=YES;
    for (UIView *obj in choosePaymentBtnsCollections) {
        obj.hidden=NO;
    }
     chooseTerminalPaymentLabel.text=@"Choose Payment Option";
}

- (IBAction)cashBtnClicked:(id)sender {
    [self resetAnimatedActionViewPayment];

}

- (IBAction)paymentCancelBtnClicked:(id)sender {
    [self resetAnimatedActionViewPayment];
    
}

-(void)addPopOverViewToTopLayer{
    //UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
    [self.view addSubview:_popOverView];
    _popOverView.hidden=YES;
}


- (IBAction)applyPercentageDiscountBtnClicked:(UIButton *)sender{

[self.view endEditing:YES];
    _popOverView.hidden=YES;
    _discountPercentageView.hidden=YES;

}

- (IBAction)applyDollarDiscountBtnClicked:(UIButton *)sender
{
    [self.view endEditing:YES];
    _popOverView.hidden=YES;
    _discountDollarView.hidden=YES;

}
- (IBAction)applyGiftBtnClicked:(UIButton *)sender{
    _popOverView.hidden=YES;
    _applyGiftView.hidden=YES;

}

- (IBAction)commonCancelBtnClicked:(UIButton *)sender
{
    [self.view endEditing:YES];
    _popOverView.hidden=YES;
    _discountDollarView.hidden=YES;
    _discountPercentageView.hidden=YES;
    _applyGiftView.hidden=YES;
}


#pragma mark AlertView Delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // the user clicked NO
    if (buttonIndex == 0) {
        // do something here...
        
    }
    _popOverView.hidden=YES;
    
}


#pragma mark UITableview datasource and delegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 93;
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if([self.screenType isEqualToString: TENDER_SCREEN])
        return [self.itemsInCart count];    //count number of row from counting array hear cataGorry is An Array
    else
        return 6;
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        BaseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil) {
            
            cell = [[[NSBundle mainBundle]loadNibNamed:@"BaseTableViewCell" owner:self options:nil]objectAtIndex:0];

           
            
        }
    
    if([self.screenType isEqualToString: TENDER_SCREEN])
    {
        NSMutableDictionary *dic = [self.itemsInCart objectAtIndex:indexPath.row];
        NSString *price = [dic valueForKey:@"Price"];
        NSString *itemId = [dic valueForKey:@"Id"];
        NSArray *cellSubViews = [cell.contentView subviews];
        for (UIView *cellItem in cellSubViews)
        {
            if([cellItem isKindOfClass:[UILabel class]] && cellItem.tag == 1)
            {
                UILabel *nameLbl = (UILabel *)cellItem;
                nameLbl.text = [dic valueForKey:@"Name"];
            }
            else if([cellItem isKindOfClass:[UILabel class]] && cellItem.tag == 2)
            {
                UILabel *noLbl = (UILabel *)cellItem;
                noLbl.text = [NSString stringWithFormat:@"No%@",itemId];
            }
            else if([cellItem isKindOfClass:[UILabel class]] && cellItem.tag == 3)
            {
                UILabel *pricelbl = (UILabel *)cellItem;
                pricelbl.text = price;
            }
            else if([cellItem isKindOfClass:[UILabel class]] && cellItem.tag == 4)
            {
                UILabel *totalLbl = (UILabel *)cellItem;
                totalLbl.text = price;
            }
            if([cellItem isKindOfClass:[UIImageView class]] && cellItem.tag == 5)
            {
                UIImageView *itemImg = (UIImageView *)cellItem;
                [itemImg setImage:[UIImage imageNamed:itemId]];
            }
        }
    }
        return cell;
    }



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    //Pushing next view
}

-(NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

@end
