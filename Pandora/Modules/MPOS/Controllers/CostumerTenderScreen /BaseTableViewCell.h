//
//  BaseTableViewCell.h
//  Pandora
//
//  Created by Ankur on 29/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewCell : UITableViewCell

@end
