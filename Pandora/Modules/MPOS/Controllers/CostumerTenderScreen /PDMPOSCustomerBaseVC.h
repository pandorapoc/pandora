//
//  PDMPOSCustomerBaseVC.h
//  Pandora
//
//  Created by Lab2 on 21/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"
#import "PDMPOSRootViewController.h"
@interface PDMPOSCustomerBaseVC : PDMPOSRootViewController
{

    IBOutlet UIButton *makeTransBtn;
    IBOutlet UIButton *modifyBtn;
    IBOutlet UIView *invoiceDetailView;
   
    
    // INVOICE DETAIL ITEMS
    IBOutlet UILabel *numOfItems;
    IBOutlet UILabel *subTotal;
    IBOutlet UILabel *shipping;
    IBOutlet UILabel *tax;
    IBOutlet UILabel *total;
    
    
    
    

    IBOutlet UILabel *chooseTerminalPaymentLabel;
    IBOutlet UIView *changeTerminalView;
    
    IBOutlet UILabel *loyalityLbl ;
    IBOutlet UILabel *customerName;

    IBOutletCollection(UIButton) NSArray *roundedBtnCollections;
    IBOutletCollection(id) NSArray *choosePaymentBtnsCollections;
}
@property (strong, nonatomic) IBOutlet UIView *actionView;
@property (strong, nonatomic) IBOutlet UIView *popOverView;
@property (strong, nonatomic) IBOutlet UIView *discountPercentageView;
@property (strong, nonatomic) IBOutlet UIView *discountDollarView;
@property (strong, nonatomic) IBOutlet UIView *applyGiftView;
@property (strong, nonatomic) IBOutlet UITableView *commonTableView;
@property (strong, nonatomic) IBOutlet UIView *tableheaderView;
@property (strong, nonatomic) IBOutlet UIView *actionViewPayment;
@property (strong, nonatomic) NSString *customerLoyalityNum;
@property(strong,nonatomic)NSMutableArray *itemsInCart;

@property(strong,nonatomic)NSString *screenType;

- (IBAction)modifyBtnClicked:(UIButton *)sender;
- (IBAction)makePaymentBtnClicked:(UIButton *)sender;

- (IBAction)applyGiftCardBtnClicked:(UIButton *)sender;
- (IBAction)applyEmployeeDiscountBtnClicked:(UIButton *)sender;
- (IBAction)postVoidBtnClicked:(UIButton *)sender;
- (IBAction)retrieveBtnClicked:(UIButton *)sender;
- (IBAction)transactionDollarDiscountBtnClicked:(UIButton *)sender;
- (IBAction)transactionPercentageDiscountBtnClicked:(UIButton *)sender;
- (IBAction)suspendBtnClicked:(UIButton *)sender;
- (IBAction)cancelBtnClicked:(UIButton *)sender;

- (IBAction)creditCardBtnClicked:(id)sender;
- (IBAction)debitCardBtnClicked:(id)sender;
- (IBAction)changeTerminalBtnClicked:(id)sender;
- (IBAction)processBtnClicked:(id)sender;
- (IBAction)cashBtnClicked:(id)sender;
- (IBAction)paymentCancelBtnClicked:(id)sender;


- (IBAction)applyPercentageDiscountBtnClicked:(UIButton *)sender;
- (IBAction)applyDollarDiscountBtnClicked:(UIButton *)sender;
- (IBAction)applyGiftBtnClicked:(UIButton *)sender;

- (IBAction)commonCancelBtnClicked:(UIButton *)sender;



@end
