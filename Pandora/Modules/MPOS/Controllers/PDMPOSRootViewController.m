//
//  MPOSLaunchViewController.m
//  Pandora
//
//  Created by Ankur on 17/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDMPOSRootViewController.h"
#import "PDMPOSCreditCardSwipeVC.h"
#import "RESideMenu.h"
#import "PDMPOSCustomerBaseVC.h"



@interface PDMPOSRootViewController ()

@end

@implementation PDMPOSRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate *)APP_DELEGATE;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];}



-(void)createCustomNavigationBar
{
    // button
    // image with text
    // label - title
    // search bar in right
    // right btn
    
    self.navigationController.navigationBar.hidden = YES;
  
    
    navBar = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 1024, 66)];
    [navBar setBackgroundColor:[UIColor colorWithRed:60.0/255 green:49.0/255 blue:66.0/255 alpha:1]];
    
    UIImage *slideImage = [UIImage imageNamed:@"menu_icon"];
    leftSlideBarBtn = [[UIButton alloc]initWithFrame:CGRectMake(5, 20,50, slideImage.size.height) ];
 
    [leftSlideBarBtn setImage:slideImage forState:UIControlStateNormal];
    [leftSlideBarBtn addTarget:self action:@selector(presentLeftMenuViewController:) forControlEvents:UIControlEventTouchUpInside];
    appDelegate.originAppType = MPOS_APP;
    [navBar addSubview:leftSlideBarBtn];

    leftBarTitleBtn = [[UIButton alloc]initWithFrame:CGRectMake(40, 20, 200, 40) ];
    [leftBarTitleBtn setTitle:@"Collections" forState:UIControlStateNormal];
    [leftBarTitleBtn setImage:[UIImage imageNamed:@"collection_icon"] forState:UIControlStateNormal];
    [leftBarTitleBtn addTarget:self action:@selector(collectionBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [navBar addSubview:leftBarTitleBtn];

    
    titleLbl = [[UILabel alloc]initWithFrame:CGRectMake(410, 20, 200, 40)];
    [titleLbl setText:@"All"];
    [titleLbl setTextColor:[UIColor whiteColor]];
    [titleLbl setTextAlignment:NSTextAlignmentCenter];
    [navBar addSubview:titleLbl];
    
    
    searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(670, 20, 250, 40)];
    [searchBar setImage:[UIImage imageNamed:@"search_icon"] forSearchBarIcon:UISearchBarIconClear state:UIControlStateHighlighted];
    searchBar.searchBarStyle = UISearchBarStyleMinimal;
    searchBar.userInteractionEnabled = YES;
    searchBar.delegate = self;
  
    [[UITextField appearanceWhenContainedIn:[searchBar class], nil] setTextColor:[UIColor whiteColor]];
  
    [navBar addSubview:searchBar];
    
    
    rightBarBtn = [[UIButton alloc]initWithFrame:CGRectMake(930, 20, 100, 40)];
    [rightBarBtn setTitle:@"Tender" forState:UIControlStateNormal];
    [rightBarBtn addTarget:self action:@selector(rightBarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [navBar addSubview:rightBarBtn];
    
    
    [navBar addSubview:titleLbl];
    
   [self.view addSubview:navBar];

    
}



-(void)collectionBtnAction:(id)sender
{
    
}

-(void)rightBarButtonAction:(id)sender
{
    PDMPOSCustomerBaseVC *tenderVC = [[PDMPOSCustomerBaseVC alloc]initWithNibName:@"PDMPOSCustomerBaseVC" bundle:nil];
    tenderVC.screenType = TENDER_SCREEN;
    [self.navigationController pushViewController:tenderVC animated:NO];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
