//
//  BOTillViewController3.m
//  Pandora
//
//  Created by Ankur on 03/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "BOTillDetailCurrencyCountVC.h"
#import "TillTableViewCell.h"

@interface BOTillDetailCurrencyCountVC ()
{
    NSArray *array1;
    NSArray *array2;

    
}
@end

@implementation BOTillDetailCurrencyCountVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [self createCustomNavigationBar];
    [self setNAvigationBar];

    array1=[[NSArray alloc]initWithObjects:@"Pennies",@"Nickels",@"Dimes",@"Quarters",@"Half-Dollar",@"1 Dollar Coins",nil];
    array2=[[NSArray alloc]initWithObjects:@"1 Dollar",@"2 Dollar",@"5 Dollar",@"10 Dollar",@"20 Dollar",@"50 Dollar",@"100 Dollar", nil];

    
    titleLbl.text=@"Till";

    
    backBtn.layer.cornerRadius = 4; // if you like rounded corners
    openRegisterBtn.layer.cornerRadius = 4;
    
    
    tabvelView1.layer.cornerRadius=10;
    tabvelView1.layer.borderColor = [UIColor colorWithWhite:0.6 alpha:0.5].CGColor;
    
    tabvelView1.layer.borderWidth = 1;
    
    tabvelView2.layer.cornerRadius=10;
    tabvelView2.layer.borderColor = [UIColor colorWithWhite:0.6 alpha:0.5].CGColor;
    
    tabvelView2.layer.borderWidth = 1;

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tabvelView1==tableView)
        return [array1 count];
    else
        return [array2 count];


}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tabvelView1==tableView) {
        TillTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil) {
            
            cell = [[[NSBundle mainBundle]loadNibNamed:@"TillTableViewCell" owner:self options:nil]objectAtIndex:0];
            
        }
        UILabel *label=(UILabel *)[cell viewWithTag:101];
        label.text=[array1 objectAtIndex:indexPath.row];;

        return cell;
    }else{
        
        TillTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil) {
            
            cell = [[[NSBundle mainBundle]loadNibNamed:@"TillTableViewCell" owner:self options:nil]objectAtIndex:0];
            
        }
        UILabel *label=(UILabel *)[cell viewWithTag:101];
        label.text=[array2 objectAtIndex:indexPath.row];
        return cell;
    }
    
}

-(void)setNAvigationBar
{
    leftSlideBarBtn.hidden = YES;
    UIButton *backButtn = [[UIButton alloc ]initWithFrame:CGRectMake(5, 20, 200, 40)];
    [backButtn setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    [backButtn setTitle:@"Dashboard" forState:UIControlStateNormal];
    [backButtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backButtn setBackgroundColor:[UIColor clearColor]];
    [backButtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [backButtn addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [navBar addSubview:backButtn];
    
    titleLbl.text = @"Till";
    rightBarBtn.hidden = YES;
}

-(void)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtnClicked:(id)sender {
}

- (IBAction)openRegisterBtnClicked:(id)sender {
}

- (IBAction)refershTotalBtnClicked:(id)sender {
}
@end
