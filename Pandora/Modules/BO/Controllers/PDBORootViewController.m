//
//  PDBORootViewController.m
//  Pandora
//
//  Created by Ila on 31/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDBORootViewController.h"

@interface PDBORootViewController ()

@end

@implementation PDBORootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate *)APP_DELEGATE;
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];


    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    
    //[self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];}



-(void)createCustomNavigationBar
{
    // button
    // image with text
    // label - title
    // search bar in right
    // right btn
    
    self.navigationController.navigationBar.hidden = YES;
    
    
    navBar = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 1024, 66)];
    [navBar setBackgroundColor:[UIColor colorWithRed:33.0/255 green:115.0/255 blue:93.0/255 alpha:1]];
    
    UIImage *slideImage = [UIImage imageNamed:@"menu_icon"];
    leftSlideBarBtn = [[UIButton alloc]initWithFrame:CGRectMake(5, 20,50, slideImage.size.height) ];
    
    [leftSlideBarBtn setImage:slideImage forState:UIControlStateNormal];
    [leftSlideBarBtn addTarget:self action:@selector(presentLeftMenuViewController:) forControlEvents:UIControlEventTouchUpInside];
      appDelegate.originAppType = BO_APP;
    [navBar addSubview:leftSlideBarBtn];
    
    
    titleLbl = [[UILabel alloc]initWithFrame:CGRectMake(410, 20, 200, 40)];
    [titleLbl setText:@"Dashboard"];
    [titleLbl setTextColor:[UIColor whiteColor]];
    [titleLbl setTextAlignment:NSTextAlignmentCenter];
    [navBar addSubview:titleLbl];
    
    
      UIImage *infoImage = [UIImage imageNamed:@"info_outline_icon"];
    
    
    rightBarBtn = [[UIButton alloc]initWithFrame:CGRectMake(930, 20, 100, 40)];
    
    [rightBarBtn setImage:infoImage forState:UIControlStateNormal];
    [rightBarBtn addTarget:self action:@selector(rightBarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [navBar addSubview:rightBarBtn];
    
    
    [navBar addSubview:titleLbl];
    
    [self.view addSubview:navBar];
    
    
}



-(void)rightBarButtonAction:(id)sender
{
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
