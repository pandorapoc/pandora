//
//  BOLaunchViewController.h
//  Pandora
//
//  Created by Ankur on 17/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDBORootViewController.h"

@interface BOLaunchViewController : PDBORootViewController<UIPopoverControllerDelegate>
{
    IBOutlet UIButton *store;
    IBOutlet UIButton *registerStore;
    IBOutlet UIButton *tillStore;
    
}

@property(strong,nonatomic)UIPopoverController *infoPopOver;

- (IBAction)storeButtonClicked:(id)sender;
- (IBAction)registerButtonClicked:(id)sender;
- (IBAction)tillButtonClicked:(id)sender;
@property(nonatomic,assign) int value;



@end
