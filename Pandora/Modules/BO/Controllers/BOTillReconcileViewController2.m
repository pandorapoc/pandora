//
//  BOTillReconcileViewController2.m
//  Pandora
//
//  Created by Ankur on 03/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "BOTillReconcileViewController2.h"
#import "BOTillReconcileListingViewController.h"

@interface BOTillReconcileViewController2 ()

@end

@implementation BOTillReconcileViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];

    [self createCustomNavigationBar];
    [self setNAvigationBar];
    titleLbl.text=@"Till";
    nextBtn.layer.cornerRadius = 4; // if you like rounded corners
    tillView.layer.cornerRadius=10;
    tillView.layer.borderColor = [UIColor colorWithWhite:0.6 alpha:0.5].CGColor;
    
    tillView.layer.borderWidth = 1;

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setNAvigationBar
{
    leftSlideBarBtn.hidden = YES;
    UIButton *backButtn = [[UIButton alloc ]initWithFrame:CGRectMake(5, 20, 200, 40)];
    [backButtn setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    [backButtn setTitle:@"Dashboard" forState:UIControlStateNormal];
    [backButtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backButtn setBackgroundColor:[UIColor clearColor]];
    [backButtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [backButtn addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [navBar addSubview:backButtn];
    
    titleLbl.text = @"Till";
    rightBarBtn.hidden = YES;
}

-(void)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextBtnClicked:(id)sender {
    BOTillReconcileListingViewController *detailReconcileList = [[BOTillReconcileListingViewController alloc]initWithNibName:@"BOTillReconcileListingViewController" bundle:nil];
    [self.navigationController pushViewController:detailReconcileList animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
