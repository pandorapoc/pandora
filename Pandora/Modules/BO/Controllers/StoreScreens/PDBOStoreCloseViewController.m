//
//  PDBOStoreCloseViewController.m
//  Pandora
//
//  Created by Ila on 04/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDBOStoreCloseViewController.h"
#import "PDBOStoreTableViewCell.h"
#import "PDBOSuccessScreen.h"

@interface PDBOStoreCloseViewController (){

    bool isProceedBtnClicked;
    //hrllo

}


@property(strong,nonatomic)NSArray *endStoreArray;
@property(strong,nonatomic)NSArray *endStoreValue;
@property(strong,nonatomic)NSArray *detailLabelArray;
@property(strong,nonatomic)NSArray *detailValueArray;

@property(strong,nonatomic)PDBOStoreTableViewCell *storeCell;


@end

@implementation PDBOStoreCloseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self createCustomNavigationBar];
    [self setNAvigationBar];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [label setTextColor:[UIColor colorWithRed:33.0/255 green:115.0/255 blue:93.0/255 alpha:1]];
    [label setFont:[UIFont fontWithName:@"Helvetica Neue" size:22]];
    self.endStoreArray = [[NSArray alloc] initWithObjects:@"Operate Fund Amount", nil];
     self.endStoreValue = [[NSArray alloc] initWithObjects:@"$1000", nil];
    
    self.detailLabelArray = [[NSArray alloc] initWithObjects:@"Cash",@"Starting Float",@"Ending Float",@"Till Loans",@"Till Pickups", nil];
    self.detailValueArray = [[NSArray alloc] initWithObjects:@"00",@"1000",@"(1000)",@"00",@"00", nil];
    
    
    
   [proceed setBackgroundColor:[UIColor colorWithRed:33.0/255 green:115.0/255 blue:93.0/255 alpha:1]];
     [proceed setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [reports setBackgroundColor:[UIColor lightGrayColor]];
    proceed.layer.masksToBounds = NO;
    proceed.layer.cornerRadius = 5; // if you like rounded corners
    reports.layer.masksToBounds = NO;
    reports.layer.cornerRadius = 5; // if you like rounded corners

    
    [closeButton setBackgroundColor:[UIColor colorWithRed:33.0/255 green:115.0/255 blue:93.0/255 alpha:1]];
    [closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    closeButton.layer.masksToBounds = NO;
    closeButton.layer.cornerRadius = 5; // if you like rounded corners

    [deatilTableView setHidden:TRUE];
    [closeButton setHidden:TRUE];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];


    
    
   

}



-(void)setNAvigationBar
{
    leftSlideBarBtn.hidden = YES;
    UIButton *backButtn = [[UIButton alloc ]initWithFrame:CGRectMake(5, 20, 200, 40)];
    [backButtn setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    [backButtn setTitle:@"Dashboard" forState:UIControlStateNormal];
    [backButtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backButtn setBackgroundColor:[UIColor clearColor]];
    [backButtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [backButtn addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [navBar addSubview:backButtn];
    
    titleLbl.text = @"Store";
    rightBarBtn.hidden = YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([detailType isEqual:@"detail"]) {
        return 5;
    }
    else
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableView.layer.cornerRadius=10;
    tableView.layer.borderColor = [UIColor colorWithWhite:0.6 alpha:0.5].CGColor;
    
    tableView.layer.borderWidth = 1;
    static NSString *popIdentifier = @"PDBOStoreTableViewCell";
    self.storeCell = [tableView dequeueReusableCellWithIdentifier:popIdentifier];
    [self.storeCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if(self.storeCell == nil){
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"PDBOStoreTableViewCell" owner:self options:nil];
        self.storeCell = [nib objectAtIndex:0];
    }
   
    if([detailType isEqual:@"detail"])
    {
        self.storeCell.cellItemLbl.text = [self.detailLabelArray objectAtIndex:indexPath.row];
        self.storeCell.textField.text = [self.detailValueArray objectAtIndex:indexPath.row];
    }
    else
    {
        self.storeCell.cellItemLbl.text = [self.endStoreArray objectAtIndex:indexPath.row];
        self.storeCell.textField.text = [self.endStoreValue objectAtIndex:indexPath.row];
    }
    
    return self.storeCell;
}
-(void)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)proceedAction:(id)sender
{
    [self.view endEditing:YES];
    isProceedBtnClicked=YES;
    
    detailType = @"detail";
    [ deatilTableView reloadData];
    [deatilTableView setHidden:false];
    
    [closeButton setHidden:false];
    [proceed setHidden:TRUE];
    [reports setHidden:TRUE];
    
}

-(IBAction)closeAction:(id)sender
{
    PDBOSuccessScreen *successScreen = [[PDBOSuccessScreen alloc] initWithNibName:@"PDBOSuccessScreen" bundle:nil];
    [self.navigationController  pushViewController:successScreen animated:YES];
    
    
}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    //Do stuff here...
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldBeginEditing");
    // if(textField == pinFeild)
    //containerView.frame = CGRectMake(211, 100, 595, 330);
 //   [self keyBoardShowing];
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    //containerView.frame = CGRectMake(211, 130, 595, 330);
    NSLog(@"textFieldDidBeginEditing");
   // [self keyBoardHiding];
    
}


-(void)keyboardWasShown :(NSNotification*)aNotification{

    [self keyBoardShowing];

}

-(void)keyboardWasHidden :(NSNotification*)aNotification{
    
    [self keyBoardHiding];

}

- (void)viewWillAppear:(BOOL)animated{

    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(keyboardWasShown:) name: UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(keyboardWasHidden:) name: UIKeyboardWillHideNotification object:nil];
}    // Called when the view is about to made visible. Default does nothing


- (void)viewWillDisappear:(BOOL)animated{
    [self.view endEditing:YES];
    [self keyBoardHiding];
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name: UIKeyboardWillShowNotification object:nil];
    [nc removeObserver:self name: UIKeyboardWillHideNotification object:nil];

}
-(void)keyBoardShowing{
    
    if ( isProceedBtnClicked) {
        [UIView animateWithDuration:0.3f animations:^{
            self.view.frame=CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-190, self.view.frame.size.width, self.view.frame.size.height);
            
        }];
        
    }
    
    
}

-(void)keyBoardHiding{
    if ( isProceedBtnClicked) {

    [UIView animateWithDuration:0.3f animations:^{
        
        self.view.frame=CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    }];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
