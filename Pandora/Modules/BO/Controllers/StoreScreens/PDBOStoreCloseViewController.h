//
//  PDBOStoreCloseViewController.h
//  Pandora
//
//  Created by Ila on 04/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDBORootViewController.h"

@interface PDBOStoreCloseViewController : PDBORootViewController<UITextFieldDelegate>
{
    IBOutlet UILabel *label;
    IBOutlet UITableView *closeDayTableView;
    IBOutlet UIButton *proceed;
    IBOutlet UIButton *reports;
    IBOutlet UITableView *deatilTableView;
     IBOutlet UIButton *closeButton;
    NSString *detailType;

}

-(IBAction)proceedAction:(id)sender;
-(IBAction)closeAction:(id)sender;

@end
