//
//  PDBOStoreViewController.m
//  Pandora
//
//  Created by Ila on 04/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDBOStoreViewController.h"
#import "PDBOStoreTableViewCell.h"

@interface PDBOStoreViewController ()
@property(strong,nonatomic)NSArray *startStoreArray;
@property(strong,nonatomic)NSArray *startStoreArrayValue;

@property(strong,nonatomic)PDBOStoreTableViewCell *storeCell;
@property (strong, nonatomic) UITextField *tmpTF;
@property(strong,nonatomic)NSString *strDate;
 @property(strong,nonatomic)NSString *dateString;
@property(strong,nonatomic)NSString *value;


@end

@implementation PDBOStoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
     [self createCustomNavigationBar];
    [self setNAvigationBar];
    NSDateFormatter *formatter;
   
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    self.dateString = [formatter stringFromDate:[NSDate date]];
    [dateBtn setTitle:self.dateString forState:UIControlStateNormal];
   [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [label setTextColor:[UIColor colorWithRed:33.0/255 green:115.0/255 blue:93.0/255 alpha:1]];
    [label setFont:[UIFont fontWithName:@"Helvetica Neue" size:22]];

    [startDayView setBackgroundColor:[UIColor colorWithRed:248.0/255 green:248.0/255 blue:248.0/255 alpha:1]];
   
    startDayView.layer.cornerRadius=10;
    startDayView.layer.borderColor = [UIColor colorWithWhite:0.6 alpha:0.5].CGColor;
    
    startDayView.layer.borderWidth = 1;
    proceed.layer.cornerRadius = 4;
    
    [dateEnter setTextColor:[UIColor colorWithRed:124.0/255 green:124.0/255 blue:124.0/255 alpha:1]];
    [fundAmount setTextColor:[UIColor colorWithRed:124.0/255 green:124.0/255 blue:124.0/255 alpha:1]];
    
    

    
    [proceed setTitleColor:[UIColor colorWithRed:33.0/255 green:115.0/255 blue:93.0/255 alpha:1] forState:UIControlStateNormal];
    [sucessText setTextColor:[UIColor colorWithRed:33.0/255 green:115.0/255 blue:93.0/255 alpha:1]];
    [detailText setTextColor:[UIColor colorWithRed:33.0/255 green:115.0/255 blue:93.0/255 alpha:1]];
    [proceed setBackgroundColor:[UIColor colorWithRed:33.0/255 green:115.0/255 blue:93.0/255 alpha:1]];
    
    [proceed setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    proceed.layer.masksToBounds = NO;
    proceed.layer.cornerRadius = 5; // if you like rounded corners

    [okImage setHidden:true];
    [sucessText setHidden:true];
    [detailText setHidden:true];
    datePicker.backgroundColor = [UIColor whiteColor];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];


}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    //Do stuff here...
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setNAvigationBar
{
    leftSlideBarBtn.hidden = YES;
    UIButton *backButtn = [[UIButton alloc ]initWithFrame:CGRectMake(5, 20, 200, 40)];
    [backButtn setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    [backButtn setTitle:@"Dashboard" forState:UIControlStateNormal];
    [backButtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backButtn setBackgroundColor:[UIColor clearColor]];
    [backButtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [backButtn addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [navBar addSubview:backButtn];
    
    titleLbl.text = @"Store";
    rightBarBtn.hidden = YES;
}

-(void)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)proceedBtn:(id)sender
{
    if([fundValue.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Please enter Fund Value"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [okImage setHidden:false];
        [sucessText setHidden:false];
        [detailText setHidden:false];
        [detailText setText:[NSString stringWithFormat:@"The Store was opened on %@ with %@$.",self.dateString,fundValue.text]];
        [self.view endEditing:YES];
    }
    
}

-(IBAction)dateBtnAction:(id)sender
{
    datePicker.hidden = NO;
    [self.view endEditing:YES];
}

- (IBAction)datePickerAction:(UIDatePicker *)datepicker
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"dd/MM/yyyy"];
    self.dateString = [dateFormatter stringFromDate:datepicker.date];
    [dateBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [dateBtn setTitle:self.dateString forState:UIControlStateNormal];
    datePicker.hidden = YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldBeginEditing");
   // if(textField == pinFeild)
       //containerView.frame = CGRectMake(211, 100, 595, 330);
    [self keyBoardShowing];
    
    self.value = fundValue.text;
    datePicker.hidden = YES;
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    //containerView.frame = CGRectMake(211, 130, 595, 330);
    NSLog(@"textFieldDidBeginEditing");
    [self keyBoardHiding];

}



-(void)keyBoardShowing{
    [UIView animateWithDuration:0.3f animations:^{
        self.view.frame=CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-20, self.view.frame.size.width, self.view.frame.size.height);

    }];

    
}

-(void)keyBoardHiding{
    [UIView animateWithDuration:0.3f animations:^{

    self.view.frame=CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+20, self.view.frame.size.width, self.view.frame.size.height);
    }];

    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
