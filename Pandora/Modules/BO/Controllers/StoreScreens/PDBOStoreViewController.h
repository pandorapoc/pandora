//
//  PDBOStoreViewController.h
//  Pandora
//
//  Created by Ila on 04/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDBORootViewController.h"

@interface PDBOStoreViewController : PDBORootViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIActionSheetDelegate>
{
    IBOutlet UILabel *label;
    IBOutlet UIView *startDayView;
    IBOutlet UIButton *proceed;
    IBOutlet UILabel *sucessText;
    IBOutlet UILabel *detailText;
    IBOutlet UIImageView *okImage;
    IBOutlet UIDatePicker *datePicker;
    IBOutlet UILabel *dateEnter;
    IBOutlet UILabel *fundAmount;
    IBOutlet UITextField *fundValue;
    
    IBOutlet UIButton *dateBtn;
}



-(IBAction)datePickerAction:(id)sender;
-(IBAction)dateBtnAction:(id)sender;

@property(assign, nonatomic)int selectedRow;


@end
