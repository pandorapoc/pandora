//
//  PDBOSuccessScreen.h
//  Pandora
//
//  Created by Ila on 05/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDBORootViewController.h"

@interface PDBOSuccessScreen : PDBORootViewController
{
    IBOutlet UILabel *sucessText;
    IBOutlet UILabel *detailText;
    IBOutlet UIImageView *okImage;
}

@end
