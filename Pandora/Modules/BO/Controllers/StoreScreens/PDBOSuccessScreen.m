//
//  PDBOSuccessScreen.m
//  Pandora
//
//  Created by Ila on 05/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDBOSuccessScreen.h"

@interface PDBOSuccessScreen ()

@end

@implementation PDBOSuccessScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createCustomNavigationBar];
    [self setNAvigationBar];

    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    
    [sucessText setFont:[UIFont fontWithName:@"Helvetica Neue" size:22]];
    
    [sucessText setTextColor:[UIColor colorWithRed:33.0/255 green:115.0/255 blue:93.0/255 alpha:1]];
    [detailText setTextColor:[UIColor colorWithRed:33.0/255 green:115.0/255 blue:93.0/255 alpha:1]];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setNAvigationBar
{
    leftSlideBarBtn.hidden = YES;
    UIButton *backButtn = [[UIButton alloc ]initWithFrame:CGRectMake(5, 20, 200, 40)];
    [backButtn setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    [backButtn setTitle:@"Dashboard" forState:UIControlStateNormal];
    [backButtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backButtn setBackgroundColor:[UIColor clearColor]];
    [backButtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [backButtn addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [navBar addSubview:backButtn];
    
    titleLbl.text = @"Store";
    rightBarBtn.hidden = YES;
}

-(void)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
