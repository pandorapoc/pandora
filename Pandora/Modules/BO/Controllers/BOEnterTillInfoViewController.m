//
//  BOTillViewController3.m
//  Pandora
//
//  Created by Ankur on 03/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "BOEnterTillInfoViewController.h"
#import "BOTillDetailCurrencyCountVC.h"

@interface BOEnterTillInfoViewController ()

@end

@implementation BOEnterTillInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [self createCustomNavigationBar];
    [self setNAvigationBar];
    titleLbl.text=@"Till";
    
    
    
    tillView.layer.cornerRadius=10;
    tillView.layer.borderColor = [UIColor colorWithWhite:0.6 alpha:0.5].CGColor;
    
    tillView.layer.borderWidth = 1;
    nextBtn.layer.cornerRadius = 4; // if you like rounded corners

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setNAvigationBar
{
    leftSlideBarBtn.hidden = YES;
    UIButton *backButtn = [[UIButton alloc ]initWithFrame:CGRectMake(5, 20, 200, 40)];
    [backButtn setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    [backButtn setTitle:@"Dashboard" forState:UIControlStateNormal];
    [backButtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backButtn setBackgroundColor:[UIColor clearColor]];
    [backButtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [backButtn addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [navBar addSubview:backButtn];
    
    titleLbl.text = @"Till";
    rightBarBtn.hidden = YES;
}

-(void)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)nextBtnClicked:(id)sender {
    
    BOTillDetailCurrencyCountVC *detailCurrencyCount = [[BOTillDetailCurrencyCountVC alloc]initWithNibName:@"BOTillDetailCurrencyCountVC" bundle:nil];
    [self.navigationController pushViewController:detailCurrencyCount animated:YES];
}
@end
