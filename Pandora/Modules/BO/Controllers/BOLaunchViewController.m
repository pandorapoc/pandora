//
//  BOLaunchViewController.m
//  Pandora
//
//  Created by Ankur on 17/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "BOLaunchViewController.h"
#import "PDBOInfoViewController.h"
#import "PDMPOSCollectionsVC.h"
#import "PDBOStorePopOverViewController.h"
#import "RegisterViewController.h"

@interface BOLaunchViewController ()

@end

@implementation BOLaunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [self createCustomNavigationBar];
  
    // Do any additional setup after loading the view from its nib.
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [store setImage:[UIImage imageNamed:@"store_icon"] forState:UIControlStateNormal];
    [registerStore setImage:[UIImage imageNamed:@"register_icon"] forState:UIControlStateNormal];
    [tillStore setImage:[UIImage imageNamed:@"till_icon"] forState:UIControlStateNormal];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];}

-(void)rightBarButtonAction:(id)sender
{
    UIButton *btn = (UIButton *)sender;
   
    PDBOInfoViewController *infoVC = [[PDBOInfoViewController alloc]initWithNibName:@"PDBOInfoViewController" bundle:nil];
    self.infoPopOver = [[UIPopoverController alloc]initWithContentViewController:infoVC];
      self.infoPopOver.delegate = self;
    self.infoPopOver.popoverContentSize = CGSizeMake(240, 100);
    infoVC.boLaunchView = self;
    [self.infoPopOver presentPopoverFromRect:btn.frame  inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
}

- (IBAction)storeButtonClicked:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
     [btn setImage:[UIImage imageNamed:@"store_green_icon"] forState:UIControlStateNormal];
    PDBOStorePopOverViewController *infoVC = [[PDBOStorePopOverViewController alloc]initWithNibName:@"PDBOStorePopOverViewController" bundle:nil];
    infoVC.type = @"store";
    infoVC.boLaunchViewController = self;
    self.infoPopOver = [[UIPopoverController alloc]initWithContentViewController:infoVC];
      self.infoPopOver.delegate = self;
     self.infoPopOver.popoverContentSize = CGSizeMake(240, 120);
    [self.infoPopOver presentPopoverFromRect:btn.frame  inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];

    //
    
}

- (IBAction)registerButtonClicked:(id)sender
{
    
    UIButton *btn = (UIButton *)sender;
    [btn setImage:[UIImage imageNamed:@"register_green_icon"] forState:UIControlStateNormal];
    PDBOStorePopOverViewController *infoVC = [[PDBOStorePopOverViewController alloc]initWithNibName:@"PDBOStorePopOverViewController" bundle:nil];
    infoVC.type = @"register";
    infoVC.boLaunchViewController = self;
    
    self.infoPopOver = [[UIPopoverController alloc]initWithContentViewController:infoVC];
    self.infoPopOver.delegate = self;
    self.infoPopOver.popoverContentSize = CGSizeMake(240, 80);
    [self.infoPopOver presentPopoverFromRect:btn.frame  inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (IBAction)tillButtonClicked:(id)sender {
    UIButton *btn = (UIButton *)sender;
    [btn setImage:[UIImage imageNamed:@"till_green_icon"] forState:UIControlStateNormal];
    PDBOStorePopOverViewController *infoVC = [[PDBOStorePopOverViewController alloc]initWithNibName:@"PDBOStorePopOverViewController" bundle:nil];
    infoVC.type = @"till";
    infoVC.boLaunchViewController = self;
    self.infoPopOver = [[UIPopoverController alloc]initWithContentViewController:infoVC];
     self.infoPopOver.delegate = self;
    self.infoPopOver.popoverContentSize = CGSizeMake(240, 80);
    [self.infoPopOver presentPopoverFromRect:btn.frame  inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}


- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    NSLog(@" alert dismissed");
    [store setImage:[UIImage imageNamed:@"store_icon"] forState:UIControlStateNormal];
    [registerStore setImage:[UIImage imageNamed:@"register_icon"] forState:UIControlStateNormal];
    [tillStore setImage:[UIImage imageNamed:@"till_icon"] forState:UIControlStateNormal];
    // do something now that it's been dismissed
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
