//
//  RegisterViewController.h
//  Pandora
//
//  Created by Lab2 on 03/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDBORootViewController.h"

@interface RegisterViewController : PDBORootViewController
{
    
    IBOutlet UIButton *openBtn;
    IBOutlet UIButton *openAllBtn;
    IBOutlet UILabel *registerSelectionLbl;
    IBOutlet UILabel *headerLbl;
    IBOutlet UITableView *registerTable;
    IBOutlet UILabel *registerStatuslbl;
    
    UIImage *openImg;
    UIImage *closeImg;
    UIImage *selectedimg;
    UIImage *unselectedImg;
    
    
    
    NSMutableArray *registerList;
    NSMutableArray *selectedList;
    NSMutableArray *openRegisters;
   
    
}

@property(strong,nonatomic)NSString *actionType;

-(IBAction)openBtnAction:(id)sender;
-(IBAction)openAllBtnAction:(id)sender;

@end
