//
//  RegisterViewController.m
//  Pandora
//
//  Created by Lab2 on 03/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()



@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [self createCustomNavigationBar];
    [self setNAvigationBar];
    [self setWidgetContent];
    openImg = [UIImage imageNamed:@"active_icon"];
    closeImg = [UIImage imageNamed:@"ideal_icon"];
    selectedimg = [UIImage imageNamed:@"select_icon"];
    unselectedImg = [UIImage imageNamed:@"select_disable_icon"];
    
    openBtn.layer.cornerRadius = 4;
    openBtn.layer.masksToBounds = YES;
    
    openAllBtn.layer.cornerRadius = 4;
    openAllBtn.layer.masksToBounds = YES;
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([self.actionType isEqualToString:OPEN_STATUS])
        registerList = [self getRegisterList];
    else
    {
        openRegisters = appDelegate.openRegisters;
        if(openRegisters == nil || [openRegisters count] == 0)
        {
            registerStatuslbl.hidden = NO;
            openBtn.hidden = YES;
            openAllBtn.hidden = YES;
            registerTable.hidden = YES;
        }
    }
    
    [registerTable reloadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    
    if([self.actionType isEqualToString:OPEN_STATUS])
    {
        NSMutableArray *openList = nil;
        appDelegate.registerList = registerList;
        for (NSMutableDictionary *dic in registerList)
        {
            if([[dic valueForKey:@"Status"] isEqualToString:OPEN_STATUS])
            {
                if(openList == nil)
                    openList = [[NSMutableArray alloc]init];
                NSMutableDictionary *newDic = [[NSMutableDictionary alloc]initWithDictionary:dic];
                 [newDic setValue:[NSNumber numberWithBool:NO] forKey:@"IsSelected"];
                [openList addObject:newDic];
            }
        }
        appDelegate.openRegisters = openList;
    }
    else
    {
        for (NSMutableDictionary *dic in selectedList)
        {
            if([[dic valueForKey:@"Status"] isEqualToString:CLOSE_STATUS])
            {
                [openRegisters removeObject:dic];
                for (NSMutableDictionary *regDic in appDelegate.registerList)
                {
                    if([[regDic valueForKey:@"RegisterId"] isEqual:[dic valueForKey:@"RegisterId"]])
                    {
                        [regDic setValue:[NSNumber numberWithBool:NO] forKey:@"IsSelected"];
                        [regDic setValue:CLOSE_STATUS forKey:@"Status"];
                    }
                }
            }
        }
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
    // Dispose of any resources that can be recreated.
}

-(void)setNAvigationBar
{
    leftSlideBarBtn.hidden = YES;
    UIButton *backButtn = [[UIButton alloc ]initWithFrame:CGRectMake(5, 20, 200, 40)];
    [backButtn setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    [backButtn setTitle:@"Dashboard" forState:UIControlStateNormal];
    [backButtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backButtn setBackgroundColor:[UIColor clearColor]];
    [backButtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [backButtn addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [navBar addSubview:backButtn];
    
    titleLbl.text = @"Register";
    rightBarBtn.hidden = YES;
}



-(void)setWidgetContent
{
    if([self.actionType isEqualToString: OPEN_STATUS])
    {
        headerLbl.text = @"Open Register";
        registerSelectionLbl.text = @"Select register(s) to open then select Open";
        [openBtn setTitle:@"Open" forState:UIControlStateNormal];
        [openAllBtn setTitle:@"Open All" forState:UIControlStateNormal];
    }
    else
    {
        headerLbl.text = @"Close Register";
        registerSelectionLbl.text = @"Select Register(s) to close then select Close";
        [openBtn setTitle:@"Close" forState:UIControlStateNormal];
        [openAllBtn setTitle:@"Close All" forState:UIControlStateNormal];
    }
}
-(void)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *secView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 761.0, 40.0)];
    secView.backgroundColor = [UIColor lightGrayColor];
    UILabel *idLbl = [[UILabel alloc]initWithFrame:CGRectMake(55, 5, 200, 30)];
    idLbl.text = @"Register";
    UILabel *statuslbl = [[UILabel alloc]initWithFrame:CGRectMake(680, 5, 200, 30)];
    statuslbl.text = @"Status";
    
    [secView addSubview:idLbl];
    [secView addSubview:statuslbl];
    
    return secView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 40;
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
      if([self.actionType isEqualToString:OPEN_STATUS])
          return [registerList count];
   
        return [openRegisters count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        
       // cell = [[[NSBundle mainBundle]loadNibNamed:@"BaseTableViewCell" owner:self options:nil]objectAtIndex:0];
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"] ;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UILabel *idLbl = [[UILabel alloc]initWithFrame:CGRectMake(55, 5, 200, 35)];
        idLbl.tag = 1;
        
        UIImageView *statusImg = [[UIImageView alloc]initWithFrame:CGRectMake(700, 5, 30, 30)];
        
      
        
        UIButton *selBtn = [[UIButton alloc]initWithFrame:CGRectMake(5, 5, 30, 30)];
        selBtn.tag = indexPath.row;
        [selBtn addTarget:self action:@selector(selBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [cell.contentView addSubview:idLbl];
        [cell.contentView addSubview:statusImg];
        [cell.contentView addSubview:selBtn];
        
    }
    NSMutableDictionary *dic = nil;
     if([self.actionType isEqualToString:OPEN_STATUS])
         dic = [registerList objectAtIndex:indexPath.row];
     else
         dic = [openRegisters objectAtIndex:indexPath.row];
    
    NSNumber *idNum = [dic valueForKey:@"RegisterId"];
    NSString *statusStr = [dic valueForKey:@"Status"];
    BOOL isSelected = [[dic valueForKey:@"IsSelected"] boolValue];
    
    NSArray *subViews = [cell.contentView subviews];
    
    for (UIView *view in subViews )
    {
        if([view isKindOfClass:[UILabel class]])
        {
            UILabel *idLbl = (UILabel *)view;
            idLbl.text = [NSString stringWithFormat:@"%@",idNum];
        }
        else if([view isKindOfClass:[UIImageView class]] )
        {
            UIImageView *statusImg = (UIImageView *)view;
            
            if([statusStr isEqualToString:OPEN_STATUS])
            {
                statusImg.image = openImg;
            }
            else
            {
                statusImg.image = closeImg;
               
            }
        }
        else if([view isKindOfClass:[UIButton class]] )
        {
            UIButton *selBtn = (UIButton *)view;
            
            if(isSelected)
            {
                [selBtn setImage:selectedimg forState:UIControlStateNormal];

               
            }
            else
            {
                [selBtn setImage:unselectedImg forState:UIControlStateNormal];

            }
        }
    }
    return cell;
}



-(void)selBtnAction:(id)sender
{
    UIButton *selBtn = (UIButton *)sender;
    
    NSUInteger tag = selBtn.tag;
    NSMutableDictionary *dic = nil;
     if([self.actionType isEqualToString:OPEN_STATUS])
         dic = [registerList objectAtIndex:tag];
    else
        dic = [openRegisters objectAtIndex:tag];
    
    BOOL isSeletected = [[dic valueForKey:@"IsSelected"] boolValue];
    if(selectedList == nil)
        selectedList = [[NSMutableArray alloc]init];
    if(!isSeletected)
    {
        [selectedList addObject:dic];
    }
    [dic setValue:[NSNumber numberWithBool:YES] forKey:@"IsSelected"];
    [registerTable reloadData];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    //Pushing next view
}


-(NSMutableArray *)getRegisterList
{
    NSMutableArray *registers = nil;
    if(appDelegate.registerList != nil && [appDelegate.registerList count] > 0)
    {
        
            registers = appDelegate.registerList;
        
    }
    else
    {
        registers = [[NSMutableArray alloc]init];
        for (int i=121 ; i<=130; i++)
        {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            [dic setValue:[NSNumber numberWithInt:i] forKey:@"RegisterId"];
            [dic setValue:CLOSE_STATUS forKey:@"Status"];
            [dic setValue:[NSNumber numberWithBool:NO] forKey:@"IsSelected"];
            [registers addObject:dic];
        }
    }
    return registers;
}

-(IBAction)openBtnAction:(id)sender
{
        if(selectedList != nil && [selectedList count] > 0)
        {
            for (NSMutableDictionary *dic in selectedList)
            {
                if([self.actionType isEqualToString:OPEN_STATUS])
                {
                    [dic setValue:OPEN_STATUS forKey:@"Status"];
                    
                }
                else
                {
                    [dic setValue:CLOSE_STATUS forKey:@"Status"];
                }
            }
        }
      [registerTable reloadData];
}
-(IBAction)openAllBtnAction:(id)sender
{
    [selectedList removeAllObjects];
    if([self.actionType isEqualToString:OPEN_STATUS])
    {
        for (NSMutableDictionary *dic in registerList)
        {
            [dic setValue:OPEN_STATUS forKey:@"Status"];
            [dic setValue:[NSNumber numberWithBool:YES] forKey:@"IsSelected"];
        }
    }
    else
    {
        for (NSMutableDictionary *dic in openRegisters)
        {
            [dic setValue:CLOSE_STATUS forKey:@"Status"];
            [dic setValue:[NSNumber numberWithBool:YES] forKey:@"IsSelected"];
            if(selectedList == nil)
                selectedList = [[NSMutableArray alloc]init];
            [selectedList addObject:dic];
        }
        
    }
    [registerTable reloadData];
}

@end
