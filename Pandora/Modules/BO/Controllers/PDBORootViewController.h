//
//  PDBORootViewController.h
//  Pandora
//
//  Created by Ila on 31/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Constants.h"


@interface PDBORootViewController : UIViewController
{
    
    UIButton *leftSlideBarBtn;
    UIButton *leftBarTitleBtn;
    UILabel *titleLbl;
    UISearchBar *searchBar;
    UIButton *rightBarBtn;
    
    UIView *navBar;
    
    AppDelegate *appDelegate;
}

-(void)createCustomNavigationBar;

-(void)collectionBtnAction:(id)sender;
-(void)rightBarButtonAction:(id)sender;

@end
