//
//  BOTillViewController3.m
//  Pandora
//
//  Created by Ankur on 03/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "BOTillReconcileListingViewController.h"
#import "TillTableViewCell.h"
#import "BOTillReconcileViewController.h"

@interface BOTillReconcileListingViewController ()
{
    NSArray *array1;
    NSArray *array2;

    
}
@end

@implementation BOTillReconcileListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [self createCustomNavigationBar];
    [self setNAvigationBar];
    array1=[[NSArray alloc]initWithObjects:@"Cash",@"Deposit Check",@"E-Check",@"Visa",@"Master Card",@"AMEX",@"Discover",@"Dinners",@"HOuse Card",@"JCB", nil];
    array2=[[NSArray alloc]initWithObjects:@"Debit Card",@"Gift Card",@"Gift Certificate",@"Mail Certificate",@"Traveller's Check",@"Coupon",@"Store Credit",@"Purchase Order",@"Money Order", nil];

    
    titleLbl.text=@"Till Reconcile";

    
   
    backBtn.layer.cornerRadius = 4; // if you like rounded corners
    openRegisterBtn.layer.cornerRadius = 4;
    
    tabvelView1.layer.cornerRadius=10;
    tabvelView1.layer.borderColor = [UIColor colorWithWhite:0.6 alpha:0.5].CGColor;
    
    tabvelView1.layer.borderWidth = 1;

    tabvelView2.layer.cornerRadius=10;
    tabvelView2.layer.borderColor = [UIColor colorWithWhite:0.6 alpha:0.5].CGColor;
    
    tabvelView2.layer.borderWidth = 1;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
    

    
}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    //Do stuff here...
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tabvelView1==tableView)
        return [array1 count];
    else
        return [array2 count];


}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tabvelView1==tableView) {
        TillTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil) {
            
            cell = [[[NSBundle mainBundle]loadNibNamed:@"TillTableViewCell" owner:self options:nil]objectAtIndex:0];
            cell.contentView.backgroundColor = [UIColor colorWithRed:248.0/255 green:248.0/255 blue:248.0/255 alpha:1];
            
        }
        UILabel *label=(UILabel *)[cell viewWithTag:101];
        label.text=[array1 objectAtIndex:indexPath.row];;

        return cell;
    }else{
        
        TillTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil) {
            
            cell = [[[NSBundle mainBundle]loadNibNamed:@"TillTableViewCell" owner:self options:nil]objectAtIndex:0];
            cell.contentView.backgroundColor = [UIColor colorWithRed:248.0/255 green:248.0/255 blue:248.0/255 alpha:1];
            
        }
        UILabel *label=(UILabel *)[cell viewWithTag:101];
        label.text=[array2 objectAtIndex:indexPath.row];
        return cell;
    }
    
}

-(void)setNAvigationBar
{
    leftSlideBarBtn.hidden = YES;
    UIButton *backButtn = [[UIButton alloc ]initWithFrame:CGRectMake(5, 20, 200, 40)];
    [backButtn setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    [backButtn setTitle:@"Dashboard" forState:UIControlStateNormal];
    [backButtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backButtn setBackgroundColor:[UIColor clearColor]];
    [backButtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [backButtn addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [navBar addSubview:backButtn];
    
    titleLbl.text = @"Till";
    rightBarBtn.hidden = YES;
}

-(void)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtnClicked:(id)sender {
}

- (IBAction)openReconcileBtnClicked:(id)sender {
    BOTillReconcileViewController *detailCurrencyCount = [[BOTillReconcileViewController alloc]initWithNibName:@"BOTillReconcileViewController" bundle:nil];
    [self.navigationController pushViewController:detailCurrencyCount animated:YES];
    
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldBeginEditing");
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"textFieldDidBeginEditing");
    
    NSUInteger currencyCount = [textField.text integerValue];
    currencyTotal +=currencyCount;
    
}
- (IBAction)refershTotalBtnClicked:(id)sender {
    
   
    reconcileCount.text = [NSString stringWithFormat:@"Total : %lu",(unsigned long)currencyTotal];
}

@end
