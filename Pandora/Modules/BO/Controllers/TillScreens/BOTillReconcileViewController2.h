//
//  BOTillReconcileViewController2.h
//  Pandora
//
//  Created by Ankur on 03/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDBORootViewController.h"

@interface BOTillReconcileViewController2 : PDBORootViewController{
    IBOutlet UIView *tillView;
    IBOutlet UIButton *nextBtn;
    
    IBOutlet UITextField *registerId;
    IBOutlet UITextField *tillId;


}

- (IBAction)nextBtnClicked:(id)sender;


@end
