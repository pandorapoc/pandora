//
//  BOTillViewController.m
//  Pandora
//
//  Created by Ankur on 03/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "BOTillReconcileViewController.h"

@interface BOTillReconcileViewController ()

@end

@implementation BOTillReconcileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];

    [self createCustomNavigationBar];
    [self setNAvigationBar];
    titleLbl.text=@"Till";
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    reconcileMsgLbl.text = [NSString stringWithFormat:@"Till %@ reconcile successfully",appDelegate.reconcileTillId];
}
-(void)viewWillDisappear:(BOOL)animated
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setNAvigationBar
{
    leftSlideBarBtn.hidden = YES;
    UIButton *backButtn = [[UIButton alloc ]initWithFrame:CGRectMake(5, 20, 200, 40)];
    [backButtn setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    [backButtn setTitle:@"Dashboard" forState:UIControlStateNormal];
    [backButtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backButtn setBackgroundColor:[UIColor clearColor]];
    [backButtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [backButtn addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [navBar addSubview:backButtn];
    
    titleLbl.text = @"Till";
    rightBarBtn.hidden = YES;
}

-(void)backButtonAction:(id)sender
{
    //[self.navigationController popViewControllerAnimated:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
