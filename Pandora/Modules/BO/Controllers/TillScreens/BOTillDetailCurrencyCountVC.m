//
//  BOTillViewController3.m
//  Pandora
//
//  Created by Ankur on 03/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "BOTillDetailCurrencyCountVC.h"
#import "TillTableViewCell.h"
#import "BOTillOpenViewController.h"

@interface BOTillDetailCurrencyCountVC ()
{
    NSArray *array1;
    NSArray *array2;

    
}
@end

@implementation BOTillDetailCurrencyCountVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [self createCustomNavigationBar];
    [self setNAvigationBar];

    array1=[[NSArray alloc]initWithObjects:@"Pennies",@"Nickels",@"Dimes",@"Quarters",@"Half-Dollar",@"1 Dollar Coins",nil];
    array2=[[NSArray alloc]initWithObjects:@"1 Dollar Bills",@"2 Dollar Bills",@"5 Dollar Bills",@"10 Dollar Bills",@"20 Dollar Bills",@"50 Dollar Bills",@"100 Dollar Bills", nil];

    
    titleLbl.text=@"Till";

    
    backBtn.layer.cornerRadius = 4; // if you like rounded corners
    refreshTotalBtn.layer.cornerRadius = 4;
    openRegisterBtn.layer.cornerRadius = 4;
    
    [tabvelView1 setBackgroundColor:[UIColor colorWithRed:248.0/255 green:248.0/255 blue:248.0/255 alpha:1]];
    [tabvelView2 setBackgroundColor:[UIColor colorWithRed:248.0/255 green:248.0/255 blue:248.0/255 alpha:1]];

    tabvelView1.layer.cornerRadius=10;
    tabvelView1.layer.borderColor = [UIColor colorWithWhite:0.6 alpha:0.5].CGColor;
    
    tabvelView1.layer.borderWidth = 1;
    
    tabvelView2.layer.cornerRadius=10;
    tabvelView2.layer.borderColor = [UIColor colorWithWhite:0.6 alpha:0.5].CGColor;
    
    tabvelView2.layer.borderWidth = 1;
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];

    

    // Do any additional setup after loading the view from its nib.
}
//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    //Do stuff here...
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tabvelView1==tableView)
        return [array1 count];
    else
        return [array2 count];


}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if (tabvelView1==tableView)
    {
        
        TillTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil)
        {
            
            cell = [[[NSBundle mainBundle]loadNibNamed:@"TillTableViewCell" owner:self options:nil]objectAtIndex:0];
            cell.contentView.backgroundColor = [UIColor colorWithRed:248.0/255 green:248.0/255 blue:248.0/255 alpha:1];
            //[cell setBackgroundColor:[UIColor colorWithRed:248.0/255 green:248.0/255 blue:248.0/255 alpha:1]];
            
        }
        UILabel *label=(UILabel *)[cell viewWithTag:101];
        label.text=[array1 objectAtIndex:indexPath.row];
        NSArray *views = [cell.contentView subviews];
        for (UIView *view in views)
        {
            if([view isKindOfClass:[UITextField class]])
            {
                UITextField *tv = (UITextField *)view;
                tv.tag = indexPath.row;
                
            }
            
        }
       
        return cell;
    }
    else
    {
        
        TillTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil)
        {
            
            cell = [[[NSBundle mainBundle]loadNibNamed:@"TillTableViewCell" owner:self options:nil]objectAtIndex:0];
            cell.contentView.backgroundColor = [UIColor colorWithRed:248.0/255 green:248.0/255 blue:248.0/255 alpha:1];
            
        }
        UILabel *label=(UILabel *)[cell viewWithTag:101];
        NSArray *views = [cell.contentView subviews];
        for (UIView *view in views)
        {
            if([view isKindOfClass:[UITextField class]])
            {
                UITextField *tv = (UITextField *)view;
                tv.tag = indexPath.row;
            }
            
        }
        label.text=[array2 objectAtIndex:indexPath.row];
        return cell;
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"did select");
}

-(void)setNAvigationBar
{
    leftSlideBarBtn.hidden = YES;
    UIButton *backButtn = [[UIButton alloc ]initWithFrame:CGRectMake(5, 20, 200, 40)];
    [backButtn setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    [backButtn setTitle:@"Dashboard" forState:UIControlStateNormal];
    [backButtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backButtn setBackgroundColor:[UIColor clearColor]];
    [backButtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [backButtn addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [navBar addSubview:backButtn];
    
    titleLbl.text = @"Till";
    rightBarBtn.hidden = YES;
}

-(void)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtnClicked:(id)sender {
    
}

- (IBAction)openTillBtnClicked:(id)sender {
    BOTillOpenViewController *detailCurrencyCount = [[BOTillOpenViewController alloc]initWithNibName:@"BOTillOpenViewController" bundle:nil];
    [self.navigationController pushViewController:detailCurrencyCount animated:YES];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldBeginEditing");
    [self keyBoardShowing:textField];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"textFieldDidBeginEditing");
    
    NSUInteger currencyCount = [textField.text integerValue];
    currencyTotal +=currencyCount;
    [self keyBoardHiding:textField];
    
}

-(void)keyBoardShowing:(UITextField *)tf
{
//    [UIView animateWithDuration:0.3f animations:^{
//      clickedTable.frame=CGRectMake(clickedTable.frame.origin.x, clickedTable.frame.origin.y-30, clickedTable.frame.size.width, clickedTable.frame.size.height);
//        
//    }];
//    
//    [clickedTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:tf.tag inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    CGPoint point = clickedTable.contentOffset;
//    point .y += clickedTable.rowHeight+(tf.tag *10);
//    clickedTable.contentOffset = point;
    [UIView animateWithDuration:0.3f animations:^{
              self.view.frame=CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-(tf.tag * 30), self.view.frame.size.width, self.view.frame.size.height);
        
            }];
    
    
}

-(void)keyBoardHiding:(UITextField *)tf
{
//    [UIView animateWithDuration:0.3f animations:^{
//        clickedTable.frame=CGRectMake(clickedTable.frame.origin.x, clickedTable.frame.origin.y+30, clickedTable.frame.size.width, clickedTable.frame.size.height);
//        
//    }];
//    
//    [clickedTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    CGPoint point = clickedTable.contentOffset;
//    point .y -= clickedTable.rowHeight;
//    clickedTable.contentOffset = point;
    [UIView animateWithDuration:0.3f animations:^{
        self.view.frame=CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+(tf.tag * 30), self.view.frame.size.width, self.view.frame.size.height);
        
    }];
    
}


- (IBAction)refershTotalBtnClicked:(id)sender {
    
   //[tabvelView1 reloadData];
    //[tabvelView2 reloadData];
    totalCurrencyLbl.text = [NSString stringWithFormat:@"Total : %lu",(unsigned long)currencyTotal];
}
@end
