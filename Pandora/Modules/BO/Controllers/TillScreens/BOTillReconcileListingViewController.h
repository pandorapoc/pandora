//
//  BOTillReconcileListingViewController.h
//  Pandora
//
//  Created by Ankur on 03/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDBORootViewController.h"

@interface BOTillReconcileListingViewController : PDBORootViewController{

    IBOutlet UIView *tillView;
    IBOutlet UIButton *backBtn;
    IBOutlet UITableView *tabvelView1;
    IBOutlet UITableView *tabvelView2;
    IBOutlet UIButton *openRegisterBtn;
    
    IBOutlet UILabel *reconcileCount;
    
    NSUInteger currencyTotal;

}
- (IBAction)backBtnClicked:(id)sender;
- (IBAction)openReconcileBtnClicked:(id)sender;
- (IBAction)refershTotalBtnClicked:(id)sender;


@end
