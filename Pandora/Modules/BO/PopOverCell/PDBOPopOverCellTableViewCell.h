//
//  PDBOPopOverCellTableViewCell.h
//  Pandora
//
//  Created by Ila on 31/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDBOPopOverCellTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *labelValue;
@property (strong, nonatomic) IBOutlet UILabel *value;



@end
