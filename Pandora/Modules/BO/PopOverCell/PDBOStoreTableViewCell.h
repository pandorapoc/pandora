//
//  PDBOStoreTableViewCell.h
//  Pandora
//
//  Created by Ila on 04/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDBOStoreTableViewCell : UITableViewCell
{
   
}

@property(strong,nonatomic)IBOutlet UILabel *cellItemLbl;
@property(strong,nonatomic)IBOutlet UITextField *textField;



@end
