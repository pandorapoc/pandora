//
//  PDBOStorePopOverViewController.m
//  Pandora
//
//  Created by Ila on 31/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDBOStorePopOverViewController.h"
#import "PDBOStoreViewController.h"
#import "RegisterViewController.h"
#import "PDBOStoreCloseViewController.h"
#import "BOEnterTillInfoViewController.h"
#import  "BOTillReconcileViewController2.h"

@interface PDBOStorePopOverViewController ()

@end

@implementation PDBOStorePopOverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if([self.type isEqual:@"store"])
        options = [[NSArray alloc] initWithObjects:@"Start of Day",@"End of Day",@"Bank Deposit", nil];
    else  if([self.type isEqual:@"register"])
        options = [[NSArray alloc]initWithObjects:@"Open Registers",@"Close Registers", nil];
    else if([self.type isEqual:@"till"])
        options = [[NSArray alloc]initWithObjects:@"Open Till",@"Reconcile Till", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [options count];    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    // Here we use the provided setImageWithURL: method to load the web image
    // Ensure you use a placeholder image otherwise cells will be initialized with no image
    
    cell.textLabel.text = [options objectAtIndex:indexPath.row];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 40;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if([self.type isEqual:@"store"])
    {
        if(indexPath.row == 0)
        {
            PDBOStoreViewController *storeViewController = [[PDBOStoreViewController alloc] initWithNibName:@"PDBOStoreViewController" bundle:nil];
            [self.boLaunchViewController.navigationController pushViewController:storeViewController animated:YES];
        }
        else if (indexPath.row == 1)
        {
            PDBOStoreCloseViewController *storeCloseViewController = [[PDBOStoreCloseViewController alloc] initWithNibName:@"PDBOStoreCloseViewController" bundle:nil];
            [self.boLaunchViewController.navigationController pushViewController:storeCloseViewController animated:YES];
        }
        
       
    }
    
    else  if([self.type isEqual:@"register"])
    {
    
        RegisterViewController *regVC = [[RegisterViewController alloc]initWithNibName:@"RegisterViewController" bundle:nil];
        if(indexPath.row == 0)
            regVC.actionType = OPEN_STATUS;
        else
            regVC.actionType = CLOSE_STATUS;
                
        [self.boLaunchViewController.navigationController pushViewController:regVC animated:YES];
    }

    else  if([self.type isEqual:@"till"])
    {
        if(indexPath.row == 0)
        {
            BOEnterTillInfoViewController *tillInfo = [[BOEnterTillInfoViewController alloc]initWithNibName:@"BOEnterTillInfoViewController" bundle:nil];
            [self.boLaunchViewController.navigationController pushViewController:tillInfo animated:YES];
        }
        
        if(indexPath.row == 1)
        {
            BOTillReconcileViewController2 *tillInfo = [[BOTillReconcileViewController2 alloc]initWithNibName:@"BOTillReconcileViewController2" bundle:nil];
            [self.boLaunchViewController.navigationController pushViewController:tillInfo animated:YES];
        }
        
        
    }

       [self.boLaunchViewController.infoPopOver dismissPopoverAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
