//
//  PDBOStorePopOverViewController.h
//  Pandora
//
//  Created by Ila on 31/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BOLaunchViewController.h"

@interface PDBOStorePopOverViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIPopoverControllerDelegate>
{
    NSArray *options;
    
    IBOutlet UITableView *optionTable;
}

@property(strong,nonatomic)NSString *type;
@property(strong,nonatomic)BOLaunchViewController *boLaunchViewController;

@end
