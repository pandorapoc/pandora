//
//  PDBOInfoViewController.m
//  Pandora
//
//  Created by Ila on 31/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDBOInfoViewController.h"

@interface PDBOInfoViewController ()

@end

@implementation PDBOInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    dateString = [formatter stringFromDate:[NSDate date]];
    titleArray = [[NSMutableArray alloc] init];
    valueArray = [[NSMutableArray alloc]init];
    
    titleArray = [[NSMutableArray alloc] initWithObjects:@"Store ID", @"User ID",@"Date" ,nil];
    valueArray = [[NSMutableArray alloc] initWithObjects:@"POS2121002",@"04241",dateString, nil];
    
    [self.infoTable setBackgroundColor:[UIColor clearColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [titleArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 28;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *popIdentifier = @"PDBOPopOverCellTableViewCell";
    self.infoCreateCell = [tableView dequeueReusableCellWithIdentifier:popIdentifier];
    [self.infoCreateCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if(self.infoCreateCell == nil){
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"PDBOPopOverCellTableViewCell" owner:self options:nil];
        self.infoCreateCell = [nib objectAtIndex:0];
    }
   
    self.infoCreateCell.labelValue.text = [titleArray objectAtIndex:indexPath.row];
    self.infoCreateCell.labelValue.backgroundColor = [UIColor clearColor];

    self.infoCreateCell.value.text = [valueArray objectAtIndex:indexPath.row];
    self.infoCreateCell.value.backgroundColor = [UIColor clearColor];
    
    return self.infoCreateCell;
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
