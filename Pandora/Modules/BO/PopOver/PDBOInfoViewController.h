//
//  PDBOInfoViewController.h
//  Pandora
//
//  Created by Ila on 31/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDBOPopOverCellTableViewCell.h"
#import "BOLaunchViewController.h"

@interface PDBOInfoViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
     NSMutableArray *titleArray;
    NSMutableArray *valueArray;
}
@property(strong, nonatomic) PDBOPopOverCellTableViewCell *infoCreateCell;
@property(strong,nonatomic) BOLaunchViewController *boLaunchView;
@property(strong,nonatomic) IBOutlet UITableView *infoTable;


@end
