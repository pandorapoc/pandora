//
//  CustomerOrderMngmntViewController.h
//  Pandora
//
//  Created by Ila on 10/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDSIMRootViewController.h"
#import "CustomerOrderTableViewCell.h"
#import "FilterProtocol.h"

@interface CustomerOrderMngmntViewController : PDSIMRootViewController<UITableViewDataSource,UITableViewDelegate,FilterProtocol>
{
    IBOutlet UIView *view1;
    IBOutlet UIView *view2;
    IBOutlet UIView *view3;
    IBOutlet UILabel *orderTitle;
    IBOutlet UIButton *search;
    IBOutlet UIButton *order;
    IBOutlet UIButton *pick;
    
     NSArray *dataArray;
    NSMutableArray *filteredArray;
    
    IBOutlet UITableView *orderListTableView;
    NSString *dataValue;
    BOOL isFilter ;
    
}
@property(strong,nonatomic)CustomerOrderTableViewCell *customerOrderCell;

-(IBAction)searchFilter:(id)sender;

@end
