//
//  CustomerOrderMngmntViewController.m
//  Pandora
//
//  Created by Ila on 10/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "CustomerOrderMngmntViewController.h"
#import "CustomerOrderTableViewCell.h"
#import "PDSIMCustomerFilterViewController.h"
#import "Constants.h"

@interface CustomerOrderMngmntViewController ()
@property(strong,nonatomic)NSMutableDictionary *dic;
@property(strong,nonatomic) NSMutableArray *dateFilterArray;

@end

@implementation CustomerOrderMngmntViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [self createCustomNavigationBar];
    [self setNAvigationBar];
    //[self getItemList];
    [orderTitle setTextColor:[UIColor colorWithRed:183.0/255 green:73.0/255 blue:39.0/255 alpha:1]];
   self.dateFilterArray = [[NSMutableArray alloc]init];
     isFilter = FALSE;
     search.layer.cornerRadius=4;
     order.layer.cornerRadius=4;
     pick.layer.cornerRadius=4;

}

-(NSArray *)parseFromFile
{
    NSString *file = [[NSBundle mainBundle] pathForResource:@"orderList" ofType:@"plist"];

    NSMutableArray *data = [NSMutableArray arrayWithContentsOfFile:file];
    NSLog(@"%@",[[data objectAtIndex:0] valueForKey:@"Transaction ID"] );
    return data;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self getItemList];
    [orderListTableView reloadData];
}


-(void )getItemList
{
    
    if(dataArray == nil || [dataArray count] == 0)
    {
        dataArray = [self parseFromFile];
    }
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   
    if(isFilter)
        return [filteredArray count];
         return [dataArray count];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *popIdentifier = @"CustomerOrderTableViewCell";
    self.customerOrderCell = [tableView dequeueReusableCellWithIdentifier:popIdentifier];
    if(self.customerOrderCell == nil){
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"CustomerOrderTableViewCell" owner:self options:nil];
        self.customerOrderCell = [nib objectAtIndex:0];
    }
    self.customerOrderCell.backgroundColor = [UIColor clearColor];
    [self.customerOrderCell setSelectionStyle:UITableViewCellSelectionStyleNone];

    
    NSMutableDictionary *dic = nil;
    if(isFilter)
         dic = [filteredArray objectAtIndex:indexPath.row];
    else
   
        dic = [dataArray objectAtIndex:indexPath.row];
    
    
    
        
        self.customerOrderCell.transactionID.text = [dic valueForKey:@"Transaction ID"];
        self.customerOrderCell.transactionType.text = [dic valueForKey:@"Transaction Type"];
        self.customerOrderCell.customerOrderNo.text = [dic valueForKey:@"Customer Order No."];
        self.customerOrderCell.from.text = [dic valueForKey:@"From"];
        self.customerOrderCell.to.text = [dic valueForKey:@"To"];
    
    
        self.customerOrderCell.date.text = [dic valueForKey:@"Date"];
    
        self.customerOrderCell.transactionStatus.text = [dic valueForKey:@"Transaction Status"];
        self.customerOrderCell.sku.text = [dic valueForKey:@"Total SKUs"];
        self.customerOrderCell.user.text = [dic valueForKey:@"User"];
  
    
    return self.customerOrderCell;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setNAvigationBar
{
    leftSlideBarBtn.hidden = YES;
    UIButton *backButtn = [[UIButton alloc ]initWithFrame:CGRectMake(5, 20, 200, 40)];
    [backButtn setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    [backButtn setTitle:@"Dashboard" forState:UIControlStateNormal];
    [backButtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backButtn setBackgroundColor:[UIColor clearColor]];
    [backButtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [backButtn addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [navBar addSubview:backButtn];
    
    titleLbl.text = @"Customer Order Management List";
    rightBarBtn.hidden = YES;
}

-(void)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)searchFilter:(id)sender
{
    PDSIMCustomerFilterViewController  *customerFilterController = [[PDSIMCustomerFilterViewController alloc] initWithNibName:@"PDSIMCustomerFilterViewController" bundle:nil];
    customerFilterController.filterDelegate = self;
    [self.navigationController pushViewController:customerFilterController animated:YES];
    
}

-(void)refreshItems:(NSDictionary *)filterValues
{
    
        isFilter = TRUE;
        if(filteredArray && filteredArray.count)
        {
            [filteredArray removeAllObjects];
        }
        if(!filteredArray)
        {
            filteredArray = [[NSMutableArray alloc]init];
        }
        NSString *orderId = [filterValues valueForKey:ORDER_ID];
        if(orderId && orderId.length)
        {
            for (NSMutableDictionary *dic in dataArray)
            {
                if([[dic valueForKey:@"Customer Order No."] isEqualToString:orderId])
                {
                    [filteredArray addObject:dic];
                    break;
                }
            }
        }
        
        else
        {
            NSDate *frmDate = [filterValues valueForKey:FROM_DATE];
            NSDate *toDate = [filterValues valueForKey:TO_DATE];
            NSString *status = [filterValues valueForKey:STATUS];
            NSString *transaction = [filterValues valueForKey:TRANSACTION];
            for (NSMutableDictionary *dic in dataArray)
            {
                NSString *itemDateStr = [dic valueForKey:@"Date"];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:DATE_FORMAT];
                
                NSDate *itemDate = [dateFormatter dateFromString:itemDateStr];
                NSString *itemTransaction = [dic valueForKey:@"Transaction Type"];
                NSString *itemStatus = [dic valueForKey:@"Transaction Status"];
                
                if([self checkTransactionType:transaction ItemSource:itemTransaction] && [self checkStatus:status ItemStatus:itemStatus] && [self isInDate:itemDate FromDate:frmDate ToDate:toDate])
                {
                    [filteredArray addObject:dic];
                }
                
                
            }
        }
//    if([filteredArray count] == 0)
//    {
//        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"No Data Found" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alertView show];
//    }
        [orderListTableView reloadData];
        
    
   
}

-(BOOL)checkTransactionType:(NSString *)source ItemSource:(NSString *)itemSource
{
    if(!source)
    {
        return TRUE;
    }
    if([source isEqual:itemSource])
    {
        return TRUE;
    }
    return FALSE;
}
-(BOOL)checkStatus:(NSString *)status ItemStatus:(NSString *)itemStatus
{
    if(!status)
    {
        return TRUE;
    }
    if([status isEqual:itemStatus])
    {
        return TRUE;
    }
    return FALSE;
}

-(BOOL)checkUser:(NSString *)user ItemUser:(NSString *)itemUser
{
    if(!user)
    {
        return TRUE;
    }
    if([user isEqual:itemUser])
    {
        return TRUE;
    }
    return FALSE;
}

-(BOOL)isInDate:(NSDate *)itemDate FromDate:(NSDate *)frmDate ToDate:(NSDate *)toDate
{
    BOOL result = FALSE;
    
    if(!toDate)
        return TRUE;
    
    BOOL toCheck = ([self compareDate:itemDate SecondDate:toDate] == DATE_LESS) || ([self compareDate:itemDate SecondDate:toDate] == DATE_EQUAL);
    BOOL fromCheck = ([self compareDate:itemDate SecondDate:frmDate] == DATE_GREATER) || ([self compareDate:itemDate SecondDate:frmDate] == DATE_EQUAL);
    
    if(  fromCheck && toCheck )
    {
        result = TRUE;
    }
    return result;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    isFilter = FALSE;
    [orderListTableView reloadData];
}

@end
