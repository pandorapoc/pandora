//
//  StoreTableViewCell.h
//  Pandora
//
//  Created by Lab2 on 11/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreTableViewCell : UITableViewCell
{
    
}

@property(strong,nonatomic)IBOutlet UILabel *orderIdLbl;
@property(strong,nonatomic)IBOutlet UILabel *sourceLbl;
@property(strong,nonatomic)IBOutlet UILabel *dateLbl;
@property(strong,nonatomic)IBOutlet UILabel *notBeforeLbl;
@property(strong,nonatomic)IBOutlet UILabel *notAfterLbl;
@property(strong,nonatomic)IBOutlet UILabel *userLbl;
@property(strong,nonatomic)IBOutlet UIView *bgView;


@property(strong,nonatomic)IBOutlet UIButton *statusBtn;
@end
