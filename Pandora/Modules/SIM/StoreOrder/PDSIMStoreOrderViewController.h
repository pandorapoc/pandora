//
//  PDSIMStoreOrderViewController.h
//  Pandora
//
//  Created by Ila on 10/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDSIMRootViewController.h"
#import "FilterProtocol.h"


@interface PDSIMStoreOrderViewController : PDSIMRootViewController<FilterProtocol,UIAlertViewDelegate>
{
    IBOutlet UIButton *filter;
    IBOutlet UIButton *create;
    IBOutlet UIButton *deleteBtn;
    IBOutlet UIButton *print;
    IBOutlet UILabel *storeLabel;
    
    IBOutlet UIButton *searchLimitBtn;
    
    IBOutlet UITableView *storeTableView;
    
    
    NSMutableArray *storeOrderArray;
    NSMutableArray *filteredArray;
    
    BOOL isFilter ;
    
}



-(IBAction)filterBtnAction:(id)sender;




@end
