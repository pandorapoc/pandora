//
//  PDSIMStoreOrderViewController.m
//  Pandora
//
//  Created by Ila on 10/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDSIMStoreOrderViewController.h"
#import "StoreTableViewCell.h"
#import "StoreFilterViewController.h"

@interface PDSIMStoreOrderViewController ()

@end

@implementation PDSIMStoreOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [self createCustomNavigationBar];
    [self setNAvigationBar];
    
    [storeLabel setTextColor:[UIColor colorWithRed:183.0/255 green:73.0/255 blue:39.0/255 alpha:1]];
   
    storeTableView.layer.cornerRadius = 4;
    
    filter.layer.cornerRadius=4;
    create.layer.cornerRadius=4;
    deleteBtn.layer.cornerRadius=4;
    print.layer.cornerRadius=4;
    searchLimitBtn.layer.cornerRadius = 4;

    isFilter = FALSE;


}

-(void)viewWillAppear:(BOOL)animated
{
    
    storeOrderArray = [self getStoreOrderList];
    [storeTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setNAvigationBar
{
    leftSlideBarBtn.hidden = YES;
    UIButton *backButtn = [[UIButton alloc ]initWithFrame:CGRectMake(5, 20, 200, 40)];
    [backButtn setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    [backButtn setTitle:@"Dashboard" forState:UIControlStateNormal];
    [backButtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backButtn setBackgroundColor:[UIColor clearColor]];
    [backButtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [backButtn addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [navBar addSubview:backButtn];
    
    titleLbl.text = @"Store Orders";
    rightBarBtn.hidden = YES;
}

-(void)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(isFilter)
        return [filteredArray count];
    return [storeOrderArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 200;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    StoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        
        cell = [[[NSBundle mainBundle]loadNibNamed:@"StoreTableViewCell" owner:self options:nil]objectAtIndex:0];
        cell.statusBtn.layer.cornerRadius = 16.0;;
        cell.backgroundColor = [UIColor clearColor];
        cell.bgView.layer.cornerRadius = 4;
        
    }
    NSMutableDictionary *dic = nil;
    if(isFilter)
         dic = [filteredArray objectAtIndex:indexPath.row];
    else
        dic = [storeOrderArray objectAtIndex:indexPath.row];
    
   cell.orderIdLbl.text = [dic valueForKey:@"OrderID"];
    cell.sourceLbl.text = [dic valueForKey:@"Source"];
    cell.dateLbl.text = [dic valueForKey:@"Date"];
    cell.notAfterLbl.text = [dic valueForKey:@"NotAfterDate"];
    cell.notBeforeLbl.text = [dic valueForKey:@"NotBeforeDate"];
    cell.userLbl.text = [dic valueForKey:@"User"];
    [cell.statusBtn setTitle:[dic valueForKey:@"Status"] forState:UIControlStateNormal];
    
    return cell;
}

-(NSMutableArray *)getStoreOrderList
{
    NSMutableArray *storeList = [self parseFromFile];
    
    return storeList;
}

-(NSMutableArray *)parseFromFile
{
    NSString *file = [[NSBundle mainBundle] pathForResource:@"StoreOrder" ofType:@"plist"];
    NSMutableArray *data = [NSMutableArray arrayWithContentsOfFile:file];
    NSLog(@"%lu", (unsigned long)data.count);
    return data;
}

-(IBAction)filterBtnAction:(id)sender
{
    StoreFilterViewController *storeViewController = [[StoreFilterViewController alloc]initWithNibName:@"StoreFilterViewController" bundle:nil];
    storeViewController.filterDelegate = self;
    [self.navigationController pushViewController:storeViewController animated:YES];
}

-(void)refreshItems:(NSDictionary *)filterValues
{
    isFilter = TRUE;
    if(filteredArray && filteredArray.count)
    {
        [filteredArray removeAllObjects];
    }
    if(!filteredArray)
    {
        filteredArray = [[NSMutableArray alloc]init];
    }
    NSString *orderId = [filterValues valueForKey:ORDER_ID];
    if(orderId && orderId.length)
    {
        for (NSMutableDictionary *dic in storeOrderArray)
        {
            if([[dic valueForKey:ORDER_ID] isEqualToString:orderId])
            {
                [filteredArray addObject:dic];
                break;
            }
        }
    }
    else
    {
        NSDate *frmDate = [filterValues valueForKey:FROM_DATE];
        NSDate *toDate = [filterValues valueForKey:TO_DATE];
        NSString *status = [filterValues valueForKey:STATUS];
        NSString *source = [filterValues valueForKey:SOURCE];
        for (NSMutableDictionary *dic in storeOrderArray)
        {
            NSString *itemDateStr = [dic valueForKey:@"Date"];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:DATE_FORMAT];
            
            NSDate *itemDate = [dateFormatter dateFromString:itemDateStr];
            NSString *itemSource = [dic valueForKey:SOURCE];
            NSString *itemStatus = [dic valueForKey:STATUS];
            
            if([self checkSource:source ItemSource:itemSource] && [self checkStatus:status ItemStatus:itemStatus] && [self isInDate:itemDate FromDate:frmDate ToDate:toDate])
            {
                [filteredArray addObject:dic];
            }
            
        }
    }
    if([filteredArray count] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"No Result Found" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
        [storeTableView reloadData];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    isFilter = FALSE;
     [storeTableView reloadData];
}

-(BOOL)checkSource:(NSString *)source ItemSource:(NSString *)itemSource
{
    if(!source)
    {
        return TRUE;
    }
    if([source isEqual:itemSource])
    {
        return TRUE;
    }
    return FALSE;
}
-(BOOL)checkStatus:(NSString *)status ItemStatus:(NSString *)itemStatus
{
    if(!status)
    {
        return TRUE;
    }
    if([status isEqual:itemStatus])
    {
        return TRUE;
    }
    return FALSE;
}
-(BOOL)isInDate:(NSDate *)itemDate FromDate:(NSDate *)frmDate ToDate:(NSDate *)toDate
{
    BOOL result = FALSE;
    
    if(!toDate)
        return TRUE;
    
    BOOL toCheck = ([self compareDate:itemDate SecondDate:toDate] == DATE_LESS) || ([self compareDate:itemDate SecondDate:toDate] == DATE_EQUAL);
    BOOL fromCheck = ([self compareDate:itemDate SecondDate:frmDate] == DATE_GREATER) || ([self compareDate:itemDate SecondDate:frmDate] == DATE_EQUAL);
    
    if(  fromCheck && toCheck )
    {
        result = TRUE;
    }
    return result;
}

@end
