//
//  StoreFilterViewController.m
//  Pandora
//
//  Created by Lab2 on 11/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "StoreFilterViewController.h"
#import "PDSIMPopOverViewController.h"
#import "Constants.h"

#define SELECTED       101
#define UNSELECTED     201

#define SUPPLIER_TYPE   @"Supplier"
#define WAREHOUSE_TYPE  @"Warehouse"
#define NONE_TYPE       @"None"

@interface StoreFilterViewController ()

@end

@implementation StoreFilterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    resetBtn.layer.cornerRadius = 4;
    resultBtn.layer.cornerRadius = 4;
    dateFilterView.layer.cornerRadius = 4;
    additionalFilterView.layer.cornerRadius = 4;
    [self createCustomNavigationBar];
    [self setNavigationBar];
   // NSString *strDate = [self getCurrentDate];
   // [frmdateBtn setTitle:DATE_FORMAT forState:UIControlStateNormal];
   // [toDateBtn setTitle:strDate forState:UIControlStateNormal];
}


-(void)setNavigationBar
{
    leftSlideBarBtn.hidden = YES;
    UIButton *backButtn = [[UIButton alloc ]initWithFrame:CGRectMake(5, 20, 200, 40)];
    [backButtn setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    [backButtn setTitle:@"Store Orders" forState:UIControlStateNormal];
    [backButtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backButtn setBackgroundColor:[UIColor clearColor]];
    [backButtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [backButtn addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [navBar addSubview:backButtn];
    
    titleLbl.text = @"Store Orders Filter";
    rightBarBtn.hidden = YES;
}

-(void)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSString *)getCurrentDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
       [dateFormatter setDateFormat:DATE_FORMAT];
//    [dateFormatter setDateStyle:NSDateFormatterFullStyle];
//    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
//    
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    NSLog(@"date string : %@",dateString);
    return dateString;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(IBAction)fromDateAction:(id)sender
{
    frmdateBtn.tag = SELECTED;
    toDateBtn.tag = UNSELECTED;
    filterDatePicker.hidden = NO;
    
   // filterDatePicker.minimumDate = [NSDate date];
}

-(IBAction)toDateAction:(id)sender
{
    NSString *frmDateStr = frmdateBtn.currentTitle;
    
    if([frmDateStr isEqual:DATE_FORMAT])
    {
        UIAlertView *dateAlert = [[UIAlertView alloc]initWithTitle:nil message:@"Please Select From Date" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [dateAlert show];
        return;
    }
    toDateBtn.tag = SELECTED;
    frmdateBtn.tag = UNSELECTED;
    filterDatePicker.hidden = NO;
   
}

-(IBAction)filterDateAction:(id)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DATE_FORMAT];
    
    NSString *frmDateStr = frmdateBtn.currentTitle;
    
    NSDate *frmDate = [dateFormatter dateFromString:frmDateStr];

   
   
   
    NSString *dateStr = [dateFormatter stringFromDate:filterDatePicker.date];
    NSDate *date = [dateFormatter dateFromString:dateStr];;
      // NSDate *frmDate = [dateFormatter dateFromString:frmDateStr];
    if(toDateBtn.tag == SELECTED && [self compareDate:date SecondDate:frmDate] == DATE_LESS)
    {
        UIAlertView *dateAlert = [[UIAlertView alloc]initWithTitle:nil message:@"To Date can not be less than From Date" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [dateAlert show];
        return;
    }
    
    [self setDate:dateStr Date:date];
    filterDatePicker.hidden = YES;
}

-(void)setDate:(NSString *)strDate Date:(NSDate *)date;
{
    if(frmdateBtn.tag == SELECTED)
    {
        selectedFromDate = date;
        [frmdateBtn setTitle:strDate forState:UIControlStateNormal];
    }
    else if(toDateBtn.tag == SELECTED)
    {
      
        selectedToDate = date;
        [toDateBtn setTitle:strDate forState:UIControlStateNormal];
    }
}


-(IBAction)statusBtnAction:(id)sender
{
    PDSIMPopOverViewController *infoVC = [[PDSIMPopOverViewController alloc]initWithNibName:@"PDSIMPopOverViewController" bundle:nil];
    infoVC.type = STORE_STATUS;
    
      infoVC.simRootVC = self;
    self.popOver = [[UIPopoverController alloc]initWithContentViewController:infoVC];
    self.popOver.delegate = self;
    self.popOver.popoverContentSize = CGSizeMake(240, 150);
      CGRect rect = CGRectMake(730,200,250.0, 150.0);
    [self.popOver presentPopoverFromRect:rect  inView:self.view permittedArrowDirections:0 animated:YES];
}
-(IBAction)sourceTypeBtnAction:(id)sender
{
    
    PDSIMPopOverViewController *infoVC = [[PDSIMPopOverViewController alloc]initWithNibName:@"PDSIMPopOverViewController" bundle:nil];
    if([seletedSourceType isEqualToString: WAREHOUSE_TYPE])
        infoVC.type = STORE_SOURCE_WH;
    else
        infoVC.type = STORE_SOURCE_SUP;
    
    infoVC.simRootVC = self;
    
    self.popOver = [[UIPopoverController alloc]initWithContentViewController:infoVC];
    self.popOver.delegate = self;
    self.popOver.popoverContentSize = CGSizeMake(240, 150);
      CGRect rect = CGRectMake(730,300,250.0, 150.0);
    [self.popOver presentPopoverFromRect:rect  inView:self.view permittedArrowDirections:0 animated:YES];
    
}
-(IBAction)segmentControlAction:(id)sender
{
    NSUInteger selectedIndex = segmentControl.selectedSegmentIndex;
    if(selectedIndex == 0)
    {
        seletedSourceType = WAREHOUSE_TYPE;
        sourceType.text = WAREHOUSE_TYPE;
        sourceType.hidden = NO;
        sourceValueBtn.hidden = NO;
        if(self.selectedWarehouse)
            [sourceValueBtn setTitle:self.selectedWarehouse forState:UIControlStateNormal];
        else
             [sourceValueBtn setTitle:DEFAULT_VALUE forState:UIControlStateNormal];
        
    }
    else  if(selectedIndex == 1)
    {
        seletedSourceType = SUPPLIER_TYPE;
        sourceType.text = SUPPLIER_TYPE;
        sourceType.hidden = NO;
        sourceValueBtn.hidden = NO;
        if(self.selectedSupplier)
            [sourceValueBtn setTitle:self.selectedSupplier forState:UIControlStateNormal];
        else
            [sourceValueBtn setTitle:DEFAULT_VALUE forState:UIControlStateNormal];
    }
    else
    {
        seletedSourceType = NONE_TYPE;
        sourceType.hidden = YES;
        sourceValueBtn.hidden = YES;
    }
}

-(void)setFilter:(NSString *)status SourceType:(NSString *)storeSoureType
{
    if(status)
        [statusValueBtn setTitle:status forState:UIControlStateNormal];
    if(storeSoureType)
        [sourceValueBtn setTitle:storeSoureType forState:UIControlStateNormal];
    
}

-(IBAction)applyFilter:(id)sender
{
    NSMutableDictionary *filterDic = [[NSMutableDictionary alloc]init];
    if(![frmdateBtn.currentTitle isEqual:DATE_FORMAT])
    {
        [filterDic setValue:selectedFromDate forKey:FROM_DATE];
    }
    if(![toDateBtn.currentTitle isEqual:DATE_FORMAT])
    {
        [filterDic setValue:selectedToDate forKey:TO_DATE];
    }
    if(orderIdTF.text && orderIdTF.text.length)
    {
        [filterDic setValue:orderIdTF.text forKey:ORDER_ID];
    }
    if(![statusValueBtn.currentTitle isEqual:DEFAULT_VALUE])
    {
        [filterDic setValue:statusValueBtn.currentTitle forKey:STATUS];
    }
    if(![sourceValueBtn.currentTitle isEqual:DEFAULT_VALUE])
    {
        [filterDic setValue:sourceValueBtn.currentTitle forKey:SOURCE];
    }
    
    [self.filterDelegate refreshItems:filterDic];
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)resetBtnAction:(id)sender
{
    seletedSourceType = NONE_TYPE;
    
    // date
    // status
    // source value
    // order id
    [frmdateBtn setTitle:DATE_FORMAT forState:UIControlStateNormal];
    [toDateBtn setTitle:DATE_FORMAT forState:UIControlStateNormal];
    orderIdTF.text = @"";
    [statusValueBtn setTitle:DEFAULT_VALUE forState:UIControlStateNormal];
    [sourceValueBtn setTitle:DEFAULT_VALUE forState:UIControlStateNormal];
}
@end
