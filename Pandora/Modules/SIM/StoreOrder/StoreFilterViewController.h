//
//  StoreFilterViewController.h
//  Pandora
//
//  Created by Lab2 on 11/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDSIMRootViewController.h"
#import "FilterProtocol.h"
#import "Constants.h"

#define DATE_FORMAT      @"dd/MM/yyyy"
#define DEFAULT_VALUE    @"Select"

@interface StoreFilterViewController : PDSIMRootViewController<UIPopoverControllerDelegate>
{
    IBOutlet UIButton *resetBtn;
    IBOutlet UIButton *resultBtn;
    IBOutlet UIView *dateFilterView;
    IBOutlet UIView *additionalFilterView;
    
    IBOutlet UIButton *frmdateBtn;
    IBOutlet UIButton *toDateBtn;
    
    IBOutlet UIDatePicker *filterDatePicker;
    
    IBOutlet UITextField *orderIdTF;
    IBOutlet UIButton *statusValueBtn;
    IBOutlet UILabel *sourceType;
    IBOutlet UIButton *sourceValueBtn;
    
    IBOutlet UISegmentedControl *segmentControl;
    
    NSString *seletedSourceType;
    NSDate *selectedFromDate;
    NSDate *selectedToDate;
    
}
@property(strong,nonatomic)UIPopoverController *popOver;
@property(strong,nonatomic)id<FilterProtocol> filterDelegate;
@property(strong, nonatomic)NSString *seletedSatus;
@property(strong,nonatomic)NSString *selectedSupplier;
@property(strong,nonatomic)NSString *selectedWarehouse;

-(IBAction)applyFilter:(id)sender;
-(IBAction)fromDateAction:(id)sender;
-(IBAction)toDateAction:(id)sender;
-(IBAction)filterDateAction:(id)sender;
-(IBAction)resetBtnAction:(id)sender;


-(IBAction)statusBtnAction:(id)sender;
-(IBAction)sourceTypeBtnAction:(id)sender;
-(IBAction)segmentControlAction:(id)sender;


-(void)setFilter:(NSString *)status SourceType:(NSString *)storeSoureType;
@end
