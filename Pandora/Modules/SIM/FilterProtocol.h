//
//  FilterProtocol.h
//  Pandora
//
//  Created by Lab2 on 13/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FilterProtocol <NSObject>


-(void)refreshItems:(NSDictionary *)filterValues;
@end
