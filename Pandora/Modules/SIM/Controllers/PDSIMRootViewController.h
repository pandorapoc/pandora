//
//  PDSIMRootViewController.h
//  Pandora
//
//  Created by Ila on 10/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

#define DATE_EQUAL    0
#define DATE_GREATER  1
#define DATE_LESS     -1



@interface PDSIMRootViewController : UIViewController
{
    
    UIButton *leftSlideBarBtn;
    UIButton *leftBarTitleBtn;
    UILabel *titleLbl;
    UISearchBar *searchBar;
    UIButton *rightBarBtn;
    
    UIView *navBar;
    
    AppDelegate *appDelegate;
}

-(void)createCustomNavigationBar;

-(void)collectionBtnAction:(id)sender;
-(void)rightBarButtonAction:(id)sender;
-(int)compareDate:(NSDate *)firstDate SecondDate:(NSDate *)secondDate;
@end
