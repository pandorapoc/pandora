//
//  ItemTableViewCell.h
//  Pandora
//
//  Created by Ankur on 12/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemTableViewCell : UITableViewCell{

    IBOutlet UIImageView *bgImageView;

}

@end
