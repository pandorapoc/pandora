//
//  ItemLookupViewController.h
//  Pandora
//
//  Created by Ankur on 10/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDSIMRootViewController.h"

@interface ItemLookupViewController : PDSIMRootViewController{

    IBOutlet UIButton *imageBtn;
    IBOutlet UIScrollView *scrollView;
    IBOutletCollection(UIView) NSArray *subviewsCollection;
}
- (IBAction)imageBtnClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *itemLookUpView;

@end
