//
//  ItemAllViewController.m
//  Pandora
//
//  Created by Ankur on 11/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "ItemAllViewController.h"
#import "ItemTableViewCell.h"
#import "ItemLookupViewController.h"
#import "PDSIMPopOverViewController.h"
#import "Constants.h"

@interface ItemAllViewController (){

    int rowsNo;
    
}

@end

@implementation ItemAllViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    rowsNo=0;
    [self createCustomNavigationBar];
    [self setNAvigationBar];
    searchBtn.layer.cornerRadius=4;
    resetBtn.layer.cornerRadius=4;
    itemsTableView.tableHeaderView=tableHeader;
    
    view1.layer.cornerRadius=4;
    view2.layer.cornerRadius=4;
    view3.layer.cornerRadius=4;

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setNAvigationBar
{
    leftSlideBarBtn.hidden = YES;
    UIButton *backButtn = [[UIButton alloc ]initWithFrame:CGRectMake(5, 20, 200, 40)];
    [backButtn setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    [backButtn setTitle:@"Dashboard" forState:UIControlStateNormal];
    [backButtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backButtn setBackgroundColor:[UIColor clearColor]];
    [backButtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backButtn addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:backButtn];
    titleLbl.text = @"Item Lookup";
    rightBarBtn.hidden = YES;
}



-(void)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark UITableview datasource and delegate methods

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    return 93;
//    
//}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

        return rowsNo;
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        
        cell = [[[NSBundle mainBundle]loadNibNamed:@"ItemTableViewCell" owner:self options:nil]objectAtIndex:0];
    }
    cell.backgroundColor=[UIColor clearColor];
   
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ItemLookupViewController  *itemViewController = [[ItemLookupViewController alloc] initWithNibName:@"ItemLookupViewController" bundle:nil];
    [self.navigationController pushViewController:itemViewController animated:YES];

    //Pushing next view
}


-(void)setFilter:(NSString *)status SourceType:(NSString *)itemSoureType
{
    if([status isEqualToString:ITEM_CLASS])
        [classBtn setTitle:itemSoureType forState:UIControlStateNormal];
    else if([status isEqualToString:ITEM_SUB_CLASS]){
        [subClassBtn setTitle:itemSoureType forState:UIControlStateNormal];
    }
    else if([status isEqualToString:ITEM_DEPARTMENT]){
        [departmentBtn setTitle:itemSoureType forState:UIControlStateNormal];
    }
    else if([status isEqualToString:ITEM_SEARCH_LIMIT]){
        [searchLimitBtn setTitle:itemSoureType forState:UIControlStateNormal];
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)searchBtnClicked:(id)sender {
    
    rowsNo=20;
    [itemsTableView reloadData];

   }

- (IBAction)searchLimitBtnClicked:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    PDSIMPopOverViewController *infoVC = [[PDSIMPopOverViewController alloc]initWithNibName:@"PDSIMPopOverViewController" bundle:nil];
    infoVC.type = ITEM_SEARCH_LIMIT;
    infoVC.simRootVC = self;
    self.infoPopOver = [[UIPopoverController alloc]initWithContentViewController:infoVC];
    self.infoPopOver.delegate = self;
    self.infoPopOver.popoverContentSize = CGSizeMake(240, 140);
    CGRect rect = CGRectMake(250,310,240.0, 140.0);

    [self.infoPopOver presentPopoverFromRect:rect  inView:self.view permittedArrowDirections:0 animated:YES];

}

- (IBAction)subClassBtnClicked:(id)sender {
    PDSIMPopOverViewController *infoVC = [[PDSIMPopOverViewController alloc]initWithNibName:@"PDSIMPopOverViewController" bundle:nil];
    infoVC.type = ITEM_SUB_CLASS;
    infoVC.simRootVC = self;
    self.infoPopOver = [[UIPopoverController alloc]initWithContentViewController:infoVC];
    self.infoPopOver.delegate = self;
    self.infoPopOver.popoverContentSize = CGSizeMake(240, 140);
    CGRect rect = CGRectMake(800,340,240.0, 140.0);
    [self.infoPopOver presentPopoverFromRect:rect  inView:self.view permittedArrowDirections:0 animated:YES];
}

- (IBAction)classBtnClicked:(id)sender {
    PDSIMPopOverViewController *infoVC = [[PDSIMPopOverViewController alloc]initWithNibName:@"PDSIMPopOverViewController" bundle:nil];
    infoVC.type = ITEM_CLASS;
    infoVC.simRootVC = self;
    self.infoPopOver = [[UIPopoverController alloc]initWithContentViewController:infoVC];
    self.infoPopOver.delegate = self;
    self.infoPopOver.popoverContentSize = CGSizeMake(240, 140);
    CGRect rect = CGRectMake(800,285,240.0, 140.0);
    [self.infoPopOver presentPopoverFromRect:rect  inView:self.view permittedArrowDirections:0 animated:YES];
}

- (IBAction)departmentBtnClicked:(id)sender {
   // UIButton *btn = (UIButton *)sender;
    PDSIMPopOverViewController *infoVC = [[PDSIMPopOverViewController alloc]initWithNibName:@"PDSIMPopOverViewController" bundle:nil];
    infoVC.type = ITEM_DEPARTMENT;
    infoVC.simRootVC = self;
    self.infoPopOver = [[UIPopoverController alloc]initWithContentViewController:infoVC];
    self.infoPopOver.delegate = self;
    self.infoPopOver.popoverContentSize = CGSizeMake(240, 140);
    CGRect rect = CGRectMake(800,250,240.0, 140.0);
    [self.infoPopOver presentPopoverFromRect:rect  inView:self.view permittedArrowDirections:0 animated:YES];
}
@end
