//
//  ItemAllViewController.h
//  Pandora
//
//  Created by Ankur on 11/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDSIMRootViewController.h"

@interface ItemAllViewController : PDSIMRootViewController<UIPopoverControllerDelegate>{

    IBOutlet UIView *view1;
    IBOutlet UIView *view2;
    IBOutlet UIView *view3;
    IBOutlet UIView *tableHeader;
    IBOutlet UITableView *itemsTableView;
    IBOutlet UIButton *resetBtn;
    IBOutlet UIButton *searchBtn;
    
    IBOutlet UIButton *searchLimitBtn;
    IBOutlet UIButton *classBtn;
    IBOutlet UIButton *subClassBtn;
    IBOutlet UIButton *departmentBtn;


}
@property(strong,nonatomic)UIPopoverController *infoPopOver;

- (IBAction)searchBtnClicked:(id)sender;
- (IBAction)searchLimitBtnClicked:(id)sender;
- (IBAction)subClassBtnClicked:(id)sender;
- (IBAction)classBtnClicked:(id)sender;
- (IBAction)departmentBtnClicked:(id)sender;
-(void)setFilter:(NSString *)status SourceType:(NSString *)itemSoureType;

@end
