//
//  ItemTableViewCell.m
//  Pandora
//
//  Created by Ankur on 12/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "ItemTableViewCell.h"

@implementation ItemTableViewCell

- (void)awakeFromNib {
    // Initialization code
    bgImageView.layer.cornerRadius=4;
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
