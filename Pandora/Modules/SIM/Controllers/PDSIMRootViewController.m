//
//  PDSIMRootViewController.m
//  Pandora
//
//  Created by Ila on 10/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDSIMRootViewController.h"
#import "Constants.h"

@interface PDSIMRootViewController ()

@end

@implementation PDSIMRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate = (AppDelegate *)APP_DELEGATE;
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];}

-(void)createCustomNavigationBar
{
    // button
    // image with text
    // label - title
    // search bar in right
    // right btn
    
    self.navigationController.navigationBar.hidden = YES;
    
    
    navBar = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 1024, 66)];
    [navBar setBackgroundColor:[UIColor colorWithRed:183.0/255 green:73.0/255 blue:39.0/255 alpha:1]];
    
    UIImage *slideImage = [UIImage imageNamed:@"menu_icon"];
    leftSlideBarBtn = [[UIButton alloc]initWithFrame:CGRectMake(5, 20,50, slideImage.size.height) ];
    
    [leftSlideBarBtn setImage:slideImage forState:UIControlStateNormal];
    [leftSlideBarBtn addTarget:self action:@selector(presentLeftMenuViewController:) forControlEvents:UIControlEventTouchUpInside];
    appDelegate.originAppType = SIM_APP;

    [navBar addSubview:leftSlideBarBtn];
    
    
    titleLbl = [[UILabel alloc]initWithFrame:CGRectMake(380, 20, 280, 40)];
    [titleLbl setText:@"Dashboard"];
    [titleLbl setTextColor:[UIColor whiteColor]];
    [titleLbl setTextAlignment:NSTextAlignmentCenter];
    [navBar addSubview:titleLbl];
    
    
    UIImage *infoImage = [UIImage imageNamed:@"info_outline_icon"];
    
    
    rightBarBtn = [[UIButton alloc]initWithFrame:CGRectMake(930, 20, 100, 40)];
    
    [rightBarBtn setImage:infoImage forState:UIControlStateNormal];
    [rightBarBtn addTarget:self action:@selector(rightBarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [navBar addSubview:rightBarBtn];
    
    
    [navBar addSubview:titleLbl];
    
    [self.view addSubview:navBar];
    
    
}



-(void)rightBarButtonAction:(id)sender
{
    
}


-(int)compareDate:(NSDate *)firstDate SecondDate:(NSDate *)secondDate
{
    int result = DATE_EQUAL;
    
    if([firstDate isEqualToDate:secondDate])
    {
        result = DATE_EQUAL;
    }
    else
    {
        NSDate *resultDate = [firstDate earlierDate:secondDate];
        if([resultDate isEqualToDate:firstDate])
        {
            result = DATE_LESS;
        }
        else
        {
            result = DATE_GREATER;
        }
    }
    return result;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
