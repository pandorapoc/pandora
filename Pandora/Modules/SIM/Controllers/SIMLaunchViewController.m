//
//  SIMLaunchViewController.m
//  Pandora
//
//  Created by Ankur on 17/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "SIMLaunchViewController.h"
#import "PDBOInfoViewController.h"
#import "PDSIMInfoViewController.h"
#import "PDSIMPopOverViewController.h"

@interface SIMLaunchViewController ()

@end

@implementation SIMLaunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [self createCustomNavigationBar];
    [heading setTextColor:[UIColor colorWithRed:183.0/255 green:73.0/255 blue:39.0/255 alpha:1]];
    customerOrderMngmnt.titleLabel.numberOfLines = 2;
    [customerOrderMngmnt setTitle:@"Customer Order \n  Management" forState:UIControlStateNormal];
    
    inventoryMngmnt.titleLabel.numberOfLines = 2;
    [inventoryMngmnt setTitle:@"   Inventory \nManagement" forState:UIControlStateNormal];
    
    shippingReceiving.titleLabel.numberOfLines = 2;
    [shippingReceiving setTitle:@"Shipping & \n Receiving" forState:UIControlStateNormal];
}

-(void)setWidgetIcon
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [shippingReceiving setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [inventoryMngmnt setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [customerOrderMngmnt setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [lookups setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    shippingReceiving.selected=NO;
    inventoryMngmnt.selected=NO;
    customerOrderMngmnt.selected=NO;
    lookups.selected=NO;
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    [super viewWillAppear:animated];
}

-(void)rightBarButtonAction:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    PDSIMInfoViewController *infoVC = [[PDSIMInfoViewController alloc]initWithNibName:@"PDSIMInfoViewController" bundle:nil];
    self.infoPopOver = [[UIPopoverController alloc]initWithContentViewController:infoVC];
    self.infoPopOver.delegate = self;
    self.infoPopOver.popoverContentSize = CGSizeMake(240, 100);
    infoVC.simLaunchView = self;
    [self.infoPopOver presentPopoverFromRect:btn.frame  inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
}


- (IBAction)lookupsButtonClicked:(id)sender {
    lookups.selected=YES;
    [lookups setTitleColor:[UIColor colorWithRed:198/255.0 green:94/255.0 blue:50/255.0 alpha:1.0] forState:UIControlStateNormal];

    UIButton *btn = (UIButton *)sender;
    PDSIMPopOverViewController *infoVC = [[PDSIMPopOverViewController alloc]initWithNibName:@"PDSIMPopOverViewController" bundle:nil];
    infoVC.type = @"lookups";
    infoVC.simLaunchViewController = self;
    self.infoPopOver = [[UIPopoverController alloc]initWithContentViewController:infoVC];
    self.infoPopOver.delegate = self;
    self.infoPopOver.popoverContentSize = CGSizeMake(240, 195);
    [self.infoPopOver presentPopoverFromRect:btn.frame  inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
    //
    
}

- (IBAction)customerOrderButtonClicked:(id)sender
{
    customerOrderMngmnt.selected=YES;
    [customerOrderMngmnt setTitleColor:[UIColor colorWithRed:198/255.0 green:94/255.0 blue:50/255.0 alpha:1.0] forState:UIControlStateNormal];

    UIButton *btn = (UIButton *)sender;
    PDSIMPopOverViewController *infoVC = [[PDSIMPopOverViewController alloc]initWithNibName:@"PDSIMPopOverViewController" bundle:nil];
    infoVC.type = @"customerorder";
    infoVC.simLaunchViewController = self;
    self.infoPopOver = [[UIPopoverController alloc]initWithContentViewController:infoVC];
    self.infoPopOver.delegate = self;
    self.infoPopOver.popoverContentSize = CGSizeMake(240, 60);
    [self.infoPopOver presentPopoverFromRect:btn.frame  inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];

}

- (IBAction)inventoryMngmntButtonClicked:(id)sender
{
    inventoryMngmnt.selected=YES;
    [inventoryMngmnt setTitleColor:[UIColor colorWithRed:198/255.0 green:94/255.0 blue:50/255.0 alpha:1.0] forState:UIControlStateNormal];

    UIButton *btn = (UIButton *)sender;
    PDSIMPopOverViewController *infoVC = [[PDSIMPopOverViewController alloc]initWithNibName:@"PDSIMPopOverViewController" bundle:nil];
    infoVC.type = @"inventory";
    infoVC.simLaunchViewController = self;
    self.infoPopOver = [[UIPopoverController alloc]initWithContentViewController:infoVC];
    self.infoPopOver.delegate = self;
    self.infoPopOver.popoverContentSize = CGSizeMake(240, 60);
    [self.infoPopOver presentPopoverFromRect:btn.frame  inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];

}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    NSLog(@" alert dismissed");
    
    [shippingReceiving setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [inventoryMngmnt setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [customerOrderMngmnt setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [lookups setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];

    
    shippingReceiving.selected=NO;
    inventoryMngmnt.selected=NO;
    customerOrderMngmnt.selected=NO;
    lookups.selected=NO;


    // do something now that it's been dismissed
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
