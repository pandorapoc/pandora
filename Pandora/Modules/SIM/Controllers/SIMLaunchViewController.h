//
//  SIMLaunchViewController.h
//  Pandora
//
//  Created by Ankur on 17/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDSIMRootViewController.h"

@interface SIMLaunchViewController : PDSIMRootViewController<UIPopoverControllerDelegate>
{
    IBOutlet UILabel *heading;
    IBOutlet UIButton *lookups;
    IBOutlet UIButton *customerOrderMngmnt;
    IBOutlet UIButton *inventoryMngmnt;
    IBOutlet UIButton *shippingReceiving;
}

@property(strong,nonatomic)UIPopoverController *infoPopOver;
- (IBAction)lookupsButtonClicked:(id)sender;
- (IBAction)customerOrderButtonClicked:(id)sender;
- (IBAction)inventoryMngmntButtonClicked:(id)sender;


@end
