//
//  ProductCatalogueViewController.m
//  Pandora
//
//  Created by Ankur on 14/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "ProductCatalogueViewController.h"
#import "ProductDetailViewController.h"

@interface ProductCatalogueViewController (){

    ProductDetailViewController *productObj;

}

@end

@implementation ProductCatalogueViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [self createCustomNavigationBar];
    //scrollview2.contentSize=view2.frame.size;
    scrollview3.contentSize=view3.frame.size;
    scrollview4.contentSize=view4.frame.size;
    scrollview5.contentSize=view5.frame.size;
    scrollview6.contentSize=view6.frame.size;
    
    smallView1.layer.cornerRadius=5;
    smallView2.layer.cornerRadius=5;
    smallView3.layer.cornerRadius=5;
    bigView.layer.cornerRadius=5;

    productObj=[[ProductDetailViewController alloc] initWithNibName:@"ProductDetailViewController" bundle:nil];
   // [self.view addSubview:productObj.view];
//    productObj.view.hidden=YES;
    
    [self setNAvigationBar];
    
}



-(void)setNAvigationBar
{
    leftBarTitleBtn.hidden = YES;
    searchBar.hidden=YES;
    titleLbl.text = @"Product Cataloge";
    rightBarBtn.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)showDressBtnClicked:(id)sender {
    
  //  PDMPOSItemDetailVC *itemDetailVC = [[PDMPOSItemDetailVC alloc]initWithNibName:@"PDMPOSItemDetailVC" bundle:nil];
    productObj.catalogueVC = self;
    self.itemDetailPopover = [[UIPopoverController alloc]initWithContentViewController:productObj];
    self.itemDetailPopover.popoverContentSize = CGSizeMake(578.0, 675.0);
    
    
    CGRect rect = CGRectMake(-150,185,578.0, 675.0);
    
    [ self.itemDetailPopover presentPopoverFromRect:rect  inView:self.view permittedArrowDirections:0 animated:YES];
  //  productObj.view.hidden=NO;

    
}


- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    NSLog(@" alert dismissed");
   //  productObj.view.hidden=YES;

    // do something now that it's been dismissed
}



@end
