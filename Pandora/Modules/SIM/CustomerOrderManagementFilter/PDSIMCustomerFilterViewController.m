//
//  PDSIMCustomerFilterViewController.m
//  Pandora
//
//  Created by Ila on 11/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDSIMCustomerFilterViewController.h"
#import "CustomerOrderMngmntViewController.h"
#import "PDSIMPopOverViewController.h"
#import "Constants.h"
#define SELECTED       101
#define UNSELECTED     201

@interface PDSIMCustomerFilterViewController ()
@property(strong,nonatomic)NSString *toDate;

@end

@implementation PDSIMCustomerFilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createCustomNavigationBar];
    [self setNAvigationBar];
    dateFilterView.layer.cornerRadius = 4;
    dateAdditionalFilterView.layer.cornerRadius = 4;
    applyBtn.layer.cornerRadius = 4;
    resetBtn.layer.cornerRadius = 4;
//    NSString *strDate = [self getCurrentDate];
//    [frmdateBtn setTitle:strDate forState:UIControlStateNormal];
//    [toDateBtn setTitle:strDate forState:UIControlStateNormal];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];

}
//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    //Do stuff here...
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setNAvigationBar
{
    leftSlideBarBtn.hidden = YES;
    UIButton *backButtn = [[UIButton alloc ]initWithFrame:CGRectMake(5, 20, 350, 40)];
    [backButtn setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    [backButtn setTitle:@"Customer Order Management List" forState:UIControlStateNormal];
    [backButtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backButtn setBackgroundColor:[UIColor clearColor]];
    [backButtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [backButtn addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [navBar addSubview:backButtn];
    
    titleLbl.text = @"Customer Order Management Filter";
    rightBarBtn.hidden = YES;
}

-(void)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(NSString *)getCurrentDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/YYYY"];
    //    [dateFormatter setDateStyle:NSDateFormatterFullStyle];
    //    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    //
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    NSLog(@"date string : %@",dateString);
    return dateString;
}

-(IBAction)fromDateAction:(id)sender
{
    frmdateBtn.tag = SELECTED;
    toDateBtn.tag = UNSELECTED;
    filterDatePicker.hidden = NO;
    // filterDatePicker.minimumDate = [NSDate date];
}

-(IBAction)toDateAction:(id)sender
{
    toDateBtn.tag = SELECTED;
    frmdateBtn.tag = UNSELECTED;
    filterDatePicker.hidden = NO;
    
}

-(IBAction)filterDateAction:(id)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DATE_FORMAT];
    
    NSString *frmDateStr = frmdateBtn.currentTitle;
    
    NSDate *frmDate = [dateFormatter dateFromString:frmDateStr];
    
    
    
    
    NSString *dateStr = [dateFormatter stringFromDate:filterDatePicker.date];
    NSDate *date = [dateFormatter dateFromString:dateStr];;
    // NSDate *frmDate = [dateFormatter dateFromString:frmDateStr];
    if(toDateBtn.tag == SELECTED && [self compareDate:date SecondDate:frmDate] == DATE_LESS)
    {
        UIAlertView *dateAlert = [[UIAlertView alloc]initWithTitle:nil message:@"To Date can not be less than From Date" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [dateAlert show];
        return;
    }
    
    [self setDate:dateStr Date:date];
    filterDatePicker.hidden = YES;
}

-(void)setDate:(NSString *)strDate Date:(NSDate *)date
{
    if(frmdateBtn.tag == SELECTED)
    {
        selectedFromDate = date;
        [frmdateBtn setTitle:strDate forState:UIControlStateNormal];
    }
    else if(toDateBtn.tag == SELECTED)
    {
        
        selectedToDate = date;
        [toDateBtn setTitle:strDate forState:UIControlStateNormal];
    }
}

//-(IBAction)applyFilter:(id)sender
//{
//    
//    NSDictionary *filterDic = [[NSDictionary alloc]initWithObjectsAndKeys:frmdateBtn.currentTitle,FROM_DATE,toDateBtn.currentTitle,TO_DATE,self.seletedSatus,STATUS,self.selectedtransactionType,TRANSACTION,customerOrderId.text,ORDER_ID, nil];
//    [self.filterDelegate refreshItems:filterDic];
//    
//    
//    [self.navigationController popViewControllerAnimated:YES];
//}

-(IBAction)applyFilter:(id)sender
{
    NSMutableDictionary *filterDic = [[NSMutableDictionary alloc]init];
    if(![frmdateBtn.currentTitle isEqual:DATE_FORMAT])
    {
        [filterDic setValue:selectedFromDate forKey:FROM_DATE];
    }
    if(![toDateBtn.currentTitle isEqual:DATE_FORMAT])
    {
        [filterDic setValue:selectedToDate forKey:TO_DATE];
    }

    if(customerOrderId.text && customerOrderId.text.length)
    {
        [filterDic setValue:customerOrderId.text forKey:ORDER_ID];
    }
    
   
    if(![statusValueBtn.currentTitle isEqual:DEFAULT_VALUE])
    {
        [filterDic setValue:statusValueBtn.currentTitle forKey:STATUS];
    }
    if(![typeValueBtn.currentTitle isEqual:DEFAULT_VALUE])
    {
        [filterDic setValue:typeValueBtn.currentTitle forKey:TRANSACTION];
    }
    
    [self.filterDelegate refreshItems:filterDic];
    [self.navigationController popViewControllerAnimated:YES];
}


-(IBAction)statusBtnAction:(id)sender
{
    PDSIMPopOverViewController *infoVC = [[PDSIMPopOverViewController alloc]initWithNibName:@"PDSIMPopOverViewController" bundle:nil];
    infoVC.type = TRANSACTION_STATUS;
    
    infoVC.simRootVC = self;
    self.popOver = [[UIPopoverController alloc]initWithContentViewController:infoVC];
    self.popOver.delegate = self;
    self.popOver.popoverContentSize = CGSizeMake(240, 150);
    CGRect rect = CGRectMake(730,200,250.0, 150.0);
    [self.popOver presentPopoverFromRect:rect  inView:self.view permittedArrowDirections:0 animated:YES];
}

-(IBAction)transactionBtnAction:(id)sender
{
    PDSIMPopOverViewController *infoVC = [[PDSIMPopOverViewController alloc]initWithNibName:@"PDSIMPopOverViewController" bundle:nil];
    infoVC.type = TRANSACTION_TYPE;
    
    infoVC.simRootVC = self;
    self.popOver = [[UIPopoverController alloc]initWithContentViewController:infoVC];
    self.popOver.delegate = self;
    self.popOver.popoverContentSize = CGSizeMake(240, 150);
    CGRect rect = CGRectMake(730,200,250.0, 150.0);
    [self.popOver presentPopoverFromRect:rect  inView:self.view permittedArrowDirections:0 animated:YES];
}

-(void)setFilter:(NSString *)status transitionType:(NSString *)transactionType
{
    if(status)
        [statusValueBtn setTitle:status forState:UIControlStateNormal];
    if(transactionType)
        [typeValueBtn setTitle:transactionType forState:UIControlStateNormal];
    
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldBeginEditing");
    [self keyBoardShowing:textField];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"textFieldDidBeginEditing");
    
    [self keyBoardHiding:textField];
    
}

-(void)keyBoardShowing:(UITextField *)tf
{
        [UIView animateWithDuration:0.3f animations:^{
        self.view.frame=CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-(tf.tag * 30), self.view.frame.size.width, self.view.frame.size.height);
        
    }];
    
    
}

-(void)keyBoardHiding:(UITextField *)tf
{
   
    [UIView animateWithDuration:0.3f animations:^{
        self.view.frame=CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+(tf.tag * 30), self.view.frame.size.width, self.view.frame.size.height);
        
    }];
    
}


-(IBAction)resetBtnAction:(id)sender
{
    
    // date
    // status
    // source value
    // order id
    [frmdateBtn setTitle:DATE_FORMAT forState:UIControlStateNormal];
    [toDateBtn setTitle:DATE_FORMAT forState:UIControlStateNormal];
    customerOrderId.text = @"";
    [statusValueBtn setTitle:DEFAULT_VALUE forState:UIControlStateNormal];
    [typeValueBtn setTitle:DEFAULT_VALUE forState:UIControlStateNormal];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
