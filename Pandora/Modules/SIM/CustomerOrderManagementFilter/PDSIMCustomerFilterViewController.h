//
//  PDSIMCustomerFilterViewController.h
//  Pandora
//
//  Created by Ila on 11/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDSIMRootViewController.h"
#import "FilterProtocol.h"

#define DATE_FORMAT      @"dd/MM/yyyy"
#define DEFAULT_VALUE    @"Select"
#define NONE_TYPE       @"None"


@interface PDSIMCustomerFilterViewController : PDSIMRootViewController<UIPopoverControllerDelegate,UITextFieldDelegate>
{
    IBOutlet UIView *dateFilterView;
    IBOutlet UIView *dateAdditionalFilterView;

    IBOutlet UIView *additionalFilterView;
    IBOutlet UIButton *frmdateBtn;
    IBOutlet UIButton *toDateBtn;
    IBOutlet UIDatePicker *filterDatePicker;
    
    IBOutlet UIButton *statusValueBtn;
    IBOutlet UIButton *typeValueBtn;
    
    IBOutlet UITextField *customerOrderId;
    IBOutlet UITextField *transactionId;
    
    IBOutlet UIButton *resetBtn;
    IBOutlet UIButton *applyBtn;

    NSDate *selectedFromDate;
    NSDate *selectedToDate;
    
}

-(IBAction)fromDateAction:(id)sender;
-(IBAction)toDateAction:(id)sender;
-(IBAction)filterDateAction:(id)sender;

-(IBAction)statusBtnAction:(id)sender;
-(IBAction)transactionBtnAction:(id)sender;


@property(strong,nonatomic)id<FilterProtocol> filterDelegate;
@property(strong,nonatomic)UIPopoverController *popOver;
@property(strong, nonatomic)NSString *seletedSatus;
@property(strong,nonatomic)NSString *selectedtransactionType;




-(IBAction)applyFilter:(id)sender;
-(IBAction)resetBtnAction:(id)sender;

-(void)setFilter:(NSString *)status transitionType:(NSString *)transactionType;


@end
