//
//  ProductCatalogueViewController.h
//  Pandora
//
//  Created by Ankur on 14/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDMPOSRootViewController.h"

@interface ProductCatalogueViewController : PDMPOSRootViewController<UIPopoverControllerDelegate>{

    IBOutlet UIScrollView *scrollview2;
    IBOutlet UIView *view2;
    IBOutlet UIScrollView *scrollview3;
    IBOutlet UIView *view3;
    IBOutlet UIScrollView *scrollview4;
    IBOutlet UIView *view4;
    IBOutlet UIScrollView *scrollview5;
    IBOutlet UIView *view5;
    IBOutlet UIScrollView *scrollview6;
    IBOutlet UIView *view6;

    IBOutlet UIView *bigView;
    IBOutlet UIView *smallView1;
    IBOutlet UIView *smallView2;
    IBOutlet UIView *smallView3;


    
}
- (IBAction)showDressBtnClicked:(id)sender;
@property(strong,nonatomic)UIPopoverController *itemDetailPopover;

@end
