//
//  PDSIMInfoViewController.h
//  Pandora
//
//  Created by Ila on 10/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDSIMPopOverTableViewCell.h"
#import "SIMLaunchViewController.h"

@interface PDSIMInfoViewController : UIViewController
{
    NSMutableArray *titleArray;
    NSMutableArray *valueArray;
}
@property(strong, nonatomic) PDSIMPopOverTableViewCell *infoCreateCell;
@property(strong,nonatomic) SIMLaunchViewController *simLaunchView;
@property(strong,nonatomic) IBOutlet UITableView *infoTable;

@end
