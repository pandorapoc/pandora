//
//  PDSIMPopOverViewController.h
//  Pandora
//
//  Created by Ila on 10/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SIMLaunchViewController.h"
#

@interface PDSIMPopOverViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIPopoverControllerDelegate>
{
    NSArray *options;
    
    IBOutlet UITableView *optionTable;
}

@property(strong,nonatomic)NSString *type;
@property(strong,nonatomic)SIMLaunchViewController *simLaunchViewController;
@property(strong,nonatomic)PDSIMRootViewController *simRootVC;

@end
