//
//  PDSIMPopOverViewController.m
//  Pandora
//
//  Created by Ila on 10/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDSIMPopOverViewController.h"
#import "CustomerOrderMngmntViewController.h"
#import "PDSIMStoreOrderViewController.h"
#import "ItemAllViewController.h"
#import "Constants.h"
#import "StoreFilterViewController.h"
#import "PDSIMCustomerFilterViewController.h"
#import "ItemAllViewController.h"
#import "ProductDetailViewController.h"

@interface PDSIMPopOverViewController ()

@end

@implementation PDSIMPopOverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if([self.type isEqual:@"lookups"])
        options = [[NSArray alloc] initWithObjects:@"Item Lookup",@"Supplier Lookup",@"Container Lookup",@"Finisher Lookup",@"Transaction History", nil];
    else  if([self.type isEqual:@"customerorder"])
        options = [[NSArray alloc]initWithObjects:@"View Customer Orders", nil];
    else if([self.type isEqual:@"inventory"])
        options = [[NSArray alloc]initWithObjects:@"Store Orders", nil];
    else if([self.type isEqual:STORE_STATUS])
         options = [[NSArray alloc]initWithObjects:@"Open",@"Pending",@"Close", nil];
    else if([self.type isEqual:TRANSACTION_STATUS])
        options = [[NSArray alloc]initWithObjects:@"Completed",@"Pending",@"In Progress", nil];
    else if([self.type isEqual:TRANSACTION_TYPE])
        options = [[NSArray alloc]initWithObjects:@"Web Order",@"Customer Order", nil];
    else if([self.type isEqual:STORE_SOURCE_WH])
         options = [[NSArray alloc]initWithObjects:@"Warehouse1",@"Warehouse2",@"Warehouse3", nil];
    else if([self.type isEqual:STORE_SOURCE_SUP])
        options = [[NSArray alloc]initWithObjects:@"Supplier1",@"Supplier2",@"Supplier3", nil];
    else if([self.type isEqual:ITEM_CLASS])
        options = [[NSArray alloc]initWithObjects:@"Class 1",@"Class 2",@"Class 3", nil];
    else if([self.type isEqual:ITEM_DEPARTMENT])
        options = [[NSArray alloc]initWithObjects:@"Department 1",@"Department 2",@"Department 3", nil];
    else if([self.type isEqual:ITEM_SEARCH_LIMIT])
        options = [[NSArray alloc]initWithObjects:@"25",@"50",@"100", nil];
    else if([self.type isEqual:ITEM_SUB_CLASS])
        options = [[NSArray alloc]initWithObjects:@"Sub Class 1",@"Sub Class 2",@"Sub Class 3", nil];
    else if([self.type isEqual:SIZE])
        options = [[NSArray alloc]initWithObjects:@"32",@"34",@"36", nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [options count];    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    // Here we use the provided setImageWithURL: method to load the web image
    // Ensure you use a placeholder image otherwise cells will be initialized with no image
   // [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    cell.textLabel.text = [options objectAtIndex:indexPath.row];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 40;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([self.type isEqual:@"customerorder"])
    {
        CustomerOrderMngmntViewController *storeViewController = [[CustomerOrderMngmntViewController alloc] initWithNibName:@"CustomerOrderMngmntCntroller" bundle:nil];
            [self.simLaunchViewController.navigationController pushViewController:storeViewController animated:YES];
         [self.simLaunchViewController.infoPopOver dismissPopoverAnimated:YES];
    }
    else if ([self.type isEqual:@"lookups"])
    {
       ItemAllViewController  *itemViewController = [[ItemAllViewController alloc] initWithNibName:@"ItemAllViewController" bundle:nil];
        [self.simLaunchViewController.navigationController pushViewController:itemViewController animated:YES];
         [self.simLaunchViewController.infoPopOver dismissPopoverAnimated:YES];
    }
    else if ([self.type isEqual:@"inventory"])
    {
        PDSIMStoreOrderViewController  *itemViewController = [[PDSIMStoreOrderViewController alloc] initWithNibName:@"PDSIMStoreOrderViewController" bundle:nil];
        [self.simLaunchViewController.navigationController pushViewController:itemViewController animated:YES];
         [self.simLaunchViewController.infoPopOver dismissPopoverAnimated:YES];
    }
    else if ([self.type isEqual:STORE_STATUS])
    {
        StoreFilterViewController *fiterVC = (StoreFilterViewController *)self.simRootVC;
        fiterVC.seletedSatus = [options objectAtIndex:indexPath.row];
         [fiterVC setFilter:fiterVC.seletedSatus SourceType:nil];
        [fiterVC.popOver dismissPopoverAnimated:YES];
    }
    
    else if ([self.type isEqual:SIZE])
    {
        ProductDetailViewController *productVC = (ProductDetailViewController *)self.simRootVC;
        productVC.selectedSize = [options objectAtIndex:indexPath.row];
        [productVC setTitleBtn:productVC.selectedSize];
        [productVC.popOver dismissPopoverAnimated:YES];
//        StoreFilterViewController *fiterVC = (StoreFilterViewController *)self.simRootVC;
//        fiterVC.seletedSatus = [options objectAtIndex:indexPath.row];
//        [fiterVC setFilter:fiterVC.seletedSatus SourceType:nil];
//        [fiterVC.popOver dismissPopoverAnimated:YES];
    }
    else if ([self.type isEqual:STORE_SOURCE_SUP])
    {
        StoreFilterViewController *fiterVC = (StoreFilterViewController *)self.simRootVC;
        fiterVC.selectedSupplier = [options objectAtIndex:indexPath.row];
        [fiterVC setFilter:nil SourceType:fiterVC.selectedSupplier];
        [fiterVC.popOver dismissPopoverAnimated:YES];

    }
    else if ([self.type isEqual:STORE_SOURCE_WH])
    {
        StoreFilterViewController *fiterVC = (StoreFilterViewController *)self.simRootVC;
        fiterVC.selectedWarehouse = [options objectAtIndex:indexPath.row];
           [fiterVC setFilter:nil SourceType:fiterVC.selectedWarehouse];
        [fiterVC.popOver dismissPopoverAnimated:YES];

    }
    else if ([self.type isEqual:TRANSACTION_STATUS])
    {
        PDSIMCustomerFilterViewController *fiterVC = (PDSIMCustomerFilterViewController *)self.simRootVC;
        fiterVC.seletedSatus = [options objectAtIndex:indexPath.row];
        [fiterVC setFilter:fiterVC.seletedSatus transitionType:nil];
        [fiterVC.popOver dismissPopoverAnimated:YES];
    }
    else if ([self.type isEqual:TRANSACTION_TYPE])
    {
        PDSIMCustomerFilterViewController *fiterVC = (PDSIMCustomerFilterViewController *)self.simRootVC;
        fiterVC.selectedtransactionType = [options objectAtIndex:indexPath.row];
        [fiterVC setFilter:nil transitionType:fiterVC.selectedtransactionType];
        [fiterVC.popOver dismissPopoverAnimated:YES];
    }
    
    
    else if ([self.type isEqual:ITEM_CLASS])
    {
        ItemAllViewController *itemObj = (ItemAllViewController *)self.simRootVC;
        [itemObj setFilter:ITEM_CLASS SourceType:[options objectAtIndex:indexPath.row]];
        [itemObj.infoPopOver dismissPopoverAnimated:YES];

       // fiterVC.selectedWarehouse = [options objectAtIndex:indexPath.row];
       // [fiterVC setFilter:nil SourceType:fiterVC.selectedWarehouse];
        
    }
    
    else if ([self.type isEqual:ITEM_SUB_CLASS])
    {
        ItemAllViewController *itemObj = (ItemAllViewController *)self.simRootVC;
        [itemObj setFilter:ITEM_SUB_CLASS SourceType:[options objectAtIndex:indexPath.row]];
        [itemObj.infoPopOver dismissPopoverAnimated:YES];
        
    }
    else if ([self.type isEqual:ITEM_DEPARTMENT])
    {
        ItemAllViewController *itemObj = (ItemAllViewController *)self.simRootVC;
        [itemObj setFilter:ITEM_DEPARTMENT SourceType:[options objectAtIndex:indexPath.row]];
        [itemObj.infoPopOver dismissPopoverAnimated:YES];
        
    }
    else if ([self.type isEqual:ITEM_SEARCH_LIMIT])
    {
        ItemAllViewController *itemObj = (ItemAllViewController *)self.simRootVC;
        [itemObj setFilter:ITEM_SEARCH_LIMIT SourceType:[options objectAtIndex:indexPath.row]];
        [itemObj.infoPopOver dismissPopoverAnimated:YES];
        
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
