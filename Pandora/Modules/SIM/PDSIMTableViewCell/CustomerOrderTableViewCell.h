//
//  CustomerOrderTableViewCell.h
//  Pandora
//
//  Created by Ila on 11/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerOrderTableViewCell : UITableViewCell
{
    IBOutlet UIImageView *whiteImage;
    
    
}
@property(strong,nonatomic)IBOutlet UILabel *transactionID;
@property(strong,nonatomic)IBOutlet UILabel *transactionType;
@property(strong,nonatomic)IBOutlet UILabel *customerOrderNo;
@property(strong,nonatomic)IBOutlet UILabel *from;
@property(strong,nonatomic)IBOutlet UILabel *to;
@property(strong,nonatomic)IBOutlet UILabel *date;
@property(strong,nonatomic)IBOutlet UILabel *transactionStatus;
@property(strong,nonatomic)IBOutlet UILabel *sku;
@property(strong,nonatomic)IBOutlet UILabel *user;


@end
