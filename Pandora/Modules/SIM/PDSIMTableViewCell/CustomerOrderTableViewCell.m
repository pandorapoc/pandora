//
//  CustomerOrderTableViewCell.m
//  Pandora
//
//  Created by Ila on 11/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "CustomerOrderTableViewCell.h"

@implementation CustomerOrderTableViewCell

- (void)awakeFromNib {
    // Initialization code
    whiteImage.layer.cornerRadius = 4;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
