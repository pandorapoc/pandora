//
//  PDSIMPopOverTableViewCell.h
//  Pandora
//
//  Created by Ila on 10/11/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDSIMPopOverTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *labelValue;
@property (strong, nonatomic) IBOutlet UILabel *value;

@end
