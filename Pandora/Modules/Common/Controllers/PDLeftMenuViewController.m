//
//  PDLeftMenuViewController.m
//  Pandora
//
//  Created by Ankur on 21/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "PDLeftMenuViewController.h"
#import "UIViewController+RESideMenu.h"
#import "PDMPOSCustomerBaseVC.h"
#import "PDMPOSCatalogueViewController.h"
#import "PDMPOSCreditCardSwipeVC.h"
#import "BOLaunchViewController.h"
#import "BOEnterTillInfoViewController.h"
#import "BOTillOpenViewController.h"
#import "BOTillReconcileViewController.h"
#import "BOTillReconcileListingViewController.h"
#import "BOTillDetailCurrencyCountVC.h"
#import "SIMLaunchViewController.h"
#import "ProductCatalogueViewController.h"
#import "ProductDetailViewController.h"
#import "ActivityViewController.h"


@interface PDLeftMenuViewController ()
@property (strong, readwrite, nonatomic) UITableView *tableView;


@end

@implementation PDLeftMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    viewHistoryBtn.layer.cornerRadius=4;
    cancelBtn.layer.cornerRadius=4;

    // Do any additional setup after loading the view.
    self.tableView = ({
     //   UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(20, (self.view.frame.size.height - 54 * 9) / 2.0f, self.view.frame.size.width, 54 * 9) style:UITableViewStylePlain];
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(20, 400, self.view.frame.size.width, 54 * 7) style:UITableViewStylePlain];
 
        tableView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.opaque = NO;
        tableView.backgroundColor = [UIColor clearColor];
        tableView.backgroundView = nil;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableView.bounces = NO;
        //tableView.scrollsToTop = NO;
        tableView;
    });
    [self.view addSubview:self.tableView];
    self.view.backgroundColor =[UIColor colorWithRed:61.0/255.0 green:50.0/255.0 blue:66.0/255.0 alpha:1];
    _popoverLeftSubView.layer.cornerRadius=4;
    
}



#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[PDMPOSCatalogueViewController alloc] init]] animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            
            break;
        case 1:
//            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[PDMPOSCreditCardSwipeVC alloc] init]] animated:YES];
////            [self.sideMenuViewController hideMenuViewController];
//            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[BOTillDetailCurrencyCountVC alloc] init]] animated:YES];
//            [self.sideMenuViewController hideMenuViewController];
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[SIMLaunchViewController alloc] init]] animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            break;

            
            break;
        case 2:
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[BOLaunchViewController alloc] init]] animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            break;
        case 3:
            //[self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"secondViewController"]] animated:YES];
            //[self.sideMenuViewController hideMenuViewController];
            
            
            break;
        case 4:
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[ProductCatalogueViewController alloc] init]] animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            break;
        case 5:
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[ActivityViewController alloc] init]] animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 54;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:21];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.highlightedTextColor = [UIColor lightGrayColor];
        cell.selectedBackgroundView = [[UIView alloc] init];
    }
    
    NSArray *titles = @[@"mPOS", @"SIM", @"Back Office", @"Settings",@"Product Catalogue",@"Activity",@"Signout"];
    NSArray *images = @[@"mpos", @"SIM", @"BO", @"Settings",@"search_icon",@"Settings",@"IconEmpty"];
    cell.textLabel.text = titles[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:images[indexPath.row]];
    
    return cell;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)viewHistoryBtnClicked:(id)sender {
    _popoverLeftView.hidden=YES;
    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[PDMPOSCustomerBaseVC alloc] init]] animated:YES];
    [self.sideMenuViewController hideMenuViewController];
    
}

- (IBAction)CancelBtnClicked:(UIButton *)sender {
    
    _popoverLeftView.hidden=YES;
}
@end
