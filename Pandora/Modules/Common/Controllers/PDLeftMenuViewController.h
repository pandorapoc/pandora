//
//  PDLeftMenuViewController.h
//  Pandora
//
//  Created by Ankur on 21/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

@interface PDLeftMenuViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, RESideMenuDelegate>{
    IBOutlet UIButton *viewHistoryBtn;
    IBOutlet UIButton *cancelBtn;
}

@property (strong, nonatomic) IBOutlet UIView *popoverLeftView;
@property (strong, nonatomic) IBOutlet UIView *popoverLeftSubView;


- (IBAction)viewHistoryBtnClicked:(id)sender;
- (IBAction)CancelBtnClicked:(UIButton *)sender;

@end
