//
//  LoginViewController.m
//  Pandora
//
//  Created by Ankur on 17/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "LoginViewController.h"
//#import "PDMPOSRootVC.h"
#import "PDLeftMenuViewController.h"
//#import "PDRightMenuViewController.h"
#import "PDMPOSCatalogueViewController.h"
#import "AppDelegate.h"
@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    loginBtn.layer.cornerRadius=4;
    // Do any additional setup after loading the view.
    
//    blurBgImageView.image=_imageBlur;
//    
//        [UIView animateWithDuration:60.0
//                             delay:0
//                            options:UIViewAnimationOptionTransitionCrossDissolve
//                        animations:^{
//                            blurBgImageView.frame=CGRectMake(-1900,blurBgImageView.frame.origin.y, blurBgImageView.frame.size.width, blurBgImageView.frame.size.height);
//                                                  }
//                         completion:nil];
    
  //  blurBgImageView.image=[UIImage imageNamed:@"gold2.jpg"];
    appDelegate = (AppDelegate *)APP_DELEGATE;

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)loginButtonClicked:(id)sender {
    
    NSString *name = usernameTf.text;
    if ([name isEqualToString:@"Michal Hische"]&&[passwordTf.text isEqualToString:@"1234"]) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
//                                                        message:@"Login Successful"
//                                                       delegate:self
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//        [alert show];
       // [self performSegueWithIdentifier:@"SelectModuleSegue" sender:nil];
       //  PDMPOSRootVC *myNewVC = [[PDMPOSRootVC alloc] init];
       //  [self.navigationController pushViewController:myNewVC animated:YES];
        
        appDelegate.userName = name;
        
        
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[[PDMPOSCatalogueViewController alloc] init]];

               
        PDLeftMenuViewController *leftMenuViewController = [[PDLeftMenuViewController alloc] init];
       // PDRightMenuViewController *rightMenuViewController = [[PDRightMenuViewController alloc] init];
        
        RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navigationController
                                                                        leftMenuViewController:leftMenuViewController
                                                                       rightMenuViewController:nil];
        sideMenuViewController.backgroundImage = [UIImage imageNamed:@"Stars"];
        sideMenuViewController.menuPreferredStatusBarStyle = 1; // UIStatusBarStyleLightContent
        sideMenuViewController.delegate = self;
        sideMenuViewController.contentViewShadowColor = [UIColor blackColor];
        sideMenuViewController.contentViewShadowOffset = CGSizeMake(0, 0);
        sideMenuViewController.contentViewShadowOpacity = 0.6;
        sideMenuViewController.contentViewShadowRadius = 12;
        sideMenuViewController.contentViewShadowEnabled = YES;
        [self.navigationController pushViewController:sideMenuViewController animated:YES];


    }
    else{
    
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Please enter correct username and password"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    
    
    
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == usernameTf) {
        [passwordTf becomeFirstResponder];
    }
    else if (textField == passwordTf) {
        [textField becomeFirstResponder];
        [self loginButtonClicked:nil];
    }
    
//    else{
//        [textField resignFirstResponder];
//    }
    return YES;
}


@end
