//
//  LoginViewController.h
//  Pandora
//
//  Created by Ankur on 17/10/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

@class AppDelegate;

@interface LoginViewController : UIViewController<RESideMenuDelegate>
{
    IBOutlet UITextField *usernameTf;
    IBOutlet UITextField *passwordTf;
    IBOutlet UIImageView *blurBgImageView;
    
    IBOutlet UIButton *loginBtn;
    
    AppDelegate *appDelegate;
    
}

@property (nonatomic,strong) UIImage *imageBlur;
- (IBAction)loginButtonClicked:(id)sender;

@end
